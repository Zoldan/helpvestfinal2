-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Out-2019 às 03:57
-- Versão do servidor: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `helpvestbackup`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `alternativas`
--

CREATE TABLE `alternativas` (
  `idalternativa` int(11) NOT NULL,
  `idpergunta` int(11) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nome`) VALUES
(1, 'Matemática'),
(2, 'Física'),
(3, 'Geografia'),
(4, 'Português'),
(5, 'Filosofia'),
(6, 'Espanhol'),
(7, 'Biologia'),
(8, 'História'),
(9, 'Química'),
(10, 'Redação'),
(11, 'Sociologia'),
(12, 'Inglês');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pasta`
--

CREATE TABLE `pasta` (
  `idpasta` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `idresumo` int(11) NOT NULL,
  `iduser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pasta`
--

INSERT INTO `pasta` (`idpasta`, `idcategoria`, `idresumo`, `iduser`) VALUES
(12, 9, 87, 2),
(13, 10, 86, 2),
(14, 3, 82, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pergunta`
--

CREATE TABLE `pergunta` (
  `idpergunta` int(11) NOT NULL,
  `idquestionario` int(11) NOT NULL,
  `texto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `postagem`
--

CREATE TABLE `postagem` (
  `id_postagem` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `conteudo` longtext NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `breve_descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `postagem`
--

INSERT INTO `postagem` (`id_postagem`, `titulo`, `conteudo`, `id_categoria`, `id_user`, `breve_descricao`) VALUES
(78, 'Reino Fungi e sua importÃ¢ncia', '<p>Os fungos s&atilde;o popularmente conhecidos porbolores, mofos, fermentos, levedos, orelhas-de-pau, trufas e cogumelos-de-chap&eacute;u (champignon).</p><p>&Eacute; um grupo bastante numeroso, formado por cerca de 200.000 esp&eacute;cies espalhadas por praticamente qualquer tipo de ambiente.</p><h2>Os Fungos e sua Import&acirc;ncia</h2><h3>Ecol&oacute;gica</h3><p>Os fungos apresentam grande variedade de modos de vida. Podem viver como&nbsp;sapr&oacute;fagos, quando obt&ecirc;m seus alimentos decompondo organismos mortos; como&nbsp;parasitas, quando se alimentam de subst&acirc;ncias que retiram dos organismos vivos nos quais se instalam, prejudicando-o ou podendo estabelecer associa&ccedil;&otilde;es&nbsp;mutual&iacute;sticas com outros organismos, em que ambos se beneficiam. Al&eacute;m desses modos mais comuns de vida, existem alguns grupos de fungos considerados&nbsp;predadores&nbsp;que capturam pequenos animais e deles se alimentam.</p><p>Em todos os casos mencionados, os fungos&nbsp;liberam enzimas digestivas para fora de seus corpos. Essas enzimas atuam imediatamente no meio org&acirc;nico no qual eles se instalam, degradando-o &agrave; mol&eacute;culas simples, que s&atilde;o absorvidas pelo fungo como uma solu&ccedil;&atilde;o aquosa.</p><p>Os fungos&nbsp;sapr&oacute;fagos s&atilde;o respons&aacute;veis por grande parte da degrada&ccedil;&atilde;o da mat&eacute;ria org&acirc;nica, propiciando a reciclagem de nutrientes. Juntamente com as bact&eacute;rias sapr&oacute;fagas, eles comp&otilde;em o grupos dos organismos decompositores, de grande import&acirc;ncia ecol&oacute;gica. No processo da decomposi&ccedil;&atilde;o, a mat&eacute;ria org&acirc;nica contida em organismos mortos &eacute; devolvida ao ambiente, podendo ser novamente utilizada por outros organismos.</p><p><img src=\"http://localhost/helpvestbackup/view/../imagensHelpvest/01c81e9b44cd86aa0bae30d66ef69baf2af43ca4.jpg\" style=\"width: 300px;\" class=\"fr-fic fr-dib\"></p>', 7, 1, 'Resumo sobre reino fungi.'),
(79, 'OperaÃ§Ãµes com nÃºmeros complexos', '<p>Os n&uacute;meros complexos s&atilde;o escritos na sua forma alg&eacute;brica da seguinte forma: a + bi, sabemos que a e b s&atilde;o n&uacute;meros reais e que o valor de a &eacute; a parte real do n&uacute;mero complexo e que o valor de bi &eacute; a parte imagin&aacute;ria do n&uacute;mero complexo.</p><p>Podemos ent&atilde;o dizer que um n&uacute;mero complexo z ser&aacute; igual a a + bi (z = a + bi).</p><p>Com esses n&uacute;meros podemos efetuar as opera&ccedil;&otilde;es de adi&ccedil;&atilde;o, subtra&ccedil;&atilde;o e multiplica&ccedil;&atilde;o, obedecendo &agrave; ordem e caracter&iacute;sticas da parte real e parte imagin&aacute;ria.</p><p><span style=\"color: rgb(97, 189, 109);\">Adi&ccedil;&atilde;o</span></p><div data-appear-top-offset=\"300\" data-google-query-id=\"CJvrybLGl-MCFQ9VwQodUSgLfQ\"><br></div><p>Dado dois n&uacute;meros complexos quaisquer z1 = a + bi e z2 = c + di, ao adicionarmos teremos:</p><p>z1 + z2&nbsp;<br>(a + bi) + (c + di)</p><p>a + bi + c + di</p><p>a + c + bi + di</p><p>a + c + (b + d)i</p><p>(a + c) + (b + d)i</p><p>Portanto, z1 + z2 = (a + c) + (b + d)i.</p><p><span style=\"color: rgb(97, 189, 109);\">Subtra&ccedil;&atilde;o</span></p><p>Dado dois n&uacute;meros complexos quaisquer z1 = a + bi e z2 = c + di, ao subtra&iacute;mos teremos:</p><p>z1 &ndash; z2&nbsp;<br>(a + bi) &ndash; (c + di)</p><p>a + bi &ndash; c &ndash; di</p><p>a &ndash; c + bi &ndash; di</p><p>(a &ndash; c) + (b &ndash; d)i</p><p>Portanto, z1 &ndash; z2 = (a &ndash; c) + (b &ndash; d)i.</p><p><span style=\"color: rgb(97, 189, 109);\">Multiplica&ccedil;&atilde;o</span></p><p>Dado dois n&uacute;meros complexos quaisquer z1 = a + bi e z2 = c + di, ao multiplicarmos teremos:</p><p>z1 . z2&nbsp;<br>(a + bi)&nbsp;<strong>x</strong> (c + di)</p><p>ac + adi + bci + bdi2&nbsp;<br>ac + adi + bci + bd (-1)&nbsp;<br>ac + adi + bci &ndash; bd&nbsp;<br>ac &ndash; bd + adi + bci&nbsp;<br>(ac &ndash; bd) + (ad + bc)i</p><p>Portanto, z1&nbsp;<strong>x</strong> z2 = (ac + bd) + (ad + bc)I.</p>', 1, 1, 'resumo sobre nÃºmeros complexos.'),
(80, 'PrÃ©-SocrÃ¡ticos', '<p>Os<strong>&nbsp;</strong>fil&oacute;sofos pr&eacute;-socr&aacute;ticos<strong>&nbsp;</strong>fazem parte do primeiro per&iacute;odo da filosofia grega. Eles desenvolveram suas teorias do s&eacute;culo VII ao V a.C.</p><p>Recebem esse nome, posto que s&atilde;o os fil&oacute;sofos que antecederam S&oacute;crates.</p><p>Os fil&oacute;sofos pr&eacute;-socr&aacute;ticos buscavam nos elementos natureza as respostas sobre a origem do ser e do mundo. Focando principalmente nos aspectos da natureza, eram chamados de &ldquo;fil&oacute;sofos da physis&rdquo;.</p><h2>S&oacute;crates</h2><p>S&oacute;crates (470 a.C-399 a.C.) foi um importante fil&oacute;sofo grego do segundo per&iacute;odo da filosofia. Nasceu em Atenas sendo considerado um dos fundadores da filosofia ocidental.</p><p>A filosofia de S&oacute;crates, baseada no di&aacute;logo, era chamada de filosofia socr&aacute;tica. Era marcada pela express&atilde;o &ldquo;<em>conhece-te a ti mesmo</em>&rdquo;, em virtude da busca da verdade atrav&eacute;s do autoconhecimento.</p><p>Ademais, da filosofia do &ldquo;di&aacute;logo&rdquo; de S&oacute;crates, destaca-se a &ldquo;mai&ecirc;utica&rdquo;, que significa literalmente &ldquo;trazer a luz&rdquo;. Esta faz rela&ccedil;&atilde;o com a ilumina&ccedil;&atilde;o da verdade que, para ele, est&aacute; contida no pr&oacute;prio ser.</p><h2>Filosofia Grega</h2><p>Para melhor entender a filosofia grega, vale lembrar como ela est&aacute; dividida:</p><ul><li>Per&iacute;odo Pr&eacute;-Socr&aacute;tico: fase naturalista</li><li>Per&iacute;odo Socr&aacute;tico: fase antropol&oacute;gica-metaf&iacute;sica</li><li>Per&iacute;odo Helen&iacute;stico: fase &eacute;tica e c&eacute;tica</li></ul><h2>Correntes ou Escolas Pr&eacute;-Socr&aacute;ticas</h2><p>Segundo o foco e o local de desenvolvimento da filosofia, o per&iacute;odo pr&eacute;-socr&aacute;tico est&aacute; dividido em Escolas ou Correntes de pensamento, a saber:</p><ul><li>Escola J&ocirc;nica: desenvolvida na col&ocirc;nia grega J&ocirc;nia, na &Aacute;sia Menor (atual Turquia), seus principais representantes s&atilde;o: Tales de Mileto, Anax&iacute;menes de Mileto, Anaximandro de Mileto e Her&aacute;clito de &Eacute;feso.</li><li>Escola Pitag&oacute;rica: tamb&eacute;m chamada de &quot;Escola It&aacute;lica&quot;, foi desenvolvida no sul da It&aacute;lia, e recebe esse nome posto que seu principal representante foi Pit&aacute;goras de Samos.</li><li>Escola Ele&aacute;tica: desenvolvida no sul da It&aacute;lia, sendo seus principais representantes: Xen&oacute;fanes de Colof&atilde;o, Parm&ecirc;nides de El&eacute;ia e Zen&atilde;o de El&eacute;ia.</li><li>Escola Atomist<strong></strong>a: tamb&eacute;m chamada de &ldquo;Atomismo&rdquo;, foi desenvolvida na regi&atilde;o da Tr&aacute;cia, sendo seus principais representantes: Dem&oacute;crito de Abdera e Leucipo de Abdera.</li></ul><h2>Principais Fil&oacute;sofos Pr&eacute;-Socr&aacute;ticos</h2><p>Segue abaixo os principais fil&oacute;sofos do per&iacute;odo pr&eacute;-socr&aacute;tico:</p><ul><li><span style=\"color: rgb(97, 189, 109);\">Tales de Mileto (624 a.C.-548 a.C.)</span><strong><span style=\"color: rgb(97, 189, 109);\">:</span></strong> nascido na cidade de Mileto, regi&atilde;o da J&ocirc;nia, Tales acreditava que a &aacute;gua era o principal elemento, ou seja, era a ess&ecirc;ncia de todas as coisas.</li><li><strong><span style=\"color: rgb(97, 189, 109);\">Anaximandro de Mileto (610 a.C.-547 a.C.):</span>&nbsp;</strong>disc&iacute;pulo de Tales nascido em Mileto, para Anaximandro o princ&iacute;pio de tudo estava no elemento denominado &ldquo;<em>&aacute;peiron</em>&rdquo;, uma esp&eacute;cie de mat&eacute;ria infinita.</li><li>Anax&iacute;menes de Mileto (588 a.C.-524 a.C.): disc&iacute;pulo de Anaximandro nascido em Mileto, para Anax&iacute;menes o princ&iacute;pio de todas as coisas estava no elemento ar.</li><li><span style=\"color: rgb(97, 189, 109);\">Her&aacute;clito de &Eacute;feso (540 a.C.-476 a.C.):</span> considerado o &ldquo;Pai da Dial&eacute;tica&rdquo;, Her&aacute;clito nasceu em &Eacute;feso e explorou a ideia do devir (fluidez das coisas). Para ele, o princ&iacute;pio de todas as coisas estava contido no elemento fogo.</li><li>Pit&aacute;goras de Samos (570 a.C.-497 a.C.): fil&oacute;sofo e matem&aacute;tico nascido na cidade de Samos, para ele os n&uacute;meros foram seus principais elementos de estudo e reflex&atilde;o, do qual se destaca o &ldquo;Teorema de Pit&aacute;goras&rdquo;.</li><li><span style=\"color: rgb(97, 189, 109);\">Xen&oacute;fanes de C&oacute;lofon: (570 a.C.-475 a.C.):</span> nascido em C&oacute;lofon, Xen&oacute;fanes foi um dos fundadores da Escola Ele&aacute;tica, se opondo contra o misticismo na filosofia e o antropomorfismo.</li><li><span style=\"color: rgb(97, 189, 109);\">Parm&ecirc;nides de El&eacute;ia (530 a.C.-460 a.C.)</span>: disc&iacute;pulo de Xen&oacute;fanes, Parm&ecirc;nides nasceu em El&eacute;ia. Focou nos conceitos de &ldquo;<em>aletheia</em>&rdquo; e &ldquo;<em>doxa</em>&rdquo;, donde o primeiro significa a luz da verdade e o segundo, &eacute; relativo &agrave; opini&atilde;o.</li><li><span style=\"color: rgb(97, 189, 109);\">Zen&atilde;o de El&eacute;ia (490 a.C.-430 a.C.)</span>: disc&iacute;pulo de Parm&ecirc;nides, Zen&atilde;o nasceu em El&eacute;ia. Foi grande defensor das ideias de seu mestre filosofando, sobretudo, acerca dos conceitos de &ldquo;Dial&eacute;tica&rdquo; e &ldquo;Paradoxo&rdquo;.</li><li>Dem&oacute;crito de Abdera (460 a.C.- 370 a.C.): nascido na cidade de Abdera, Dem&oacute;crito foi disc&iacute;pulo de Leucipo. Para ele, o &aacute;tomo era o princ&iacute;pio de todas as coisas, desenvolvendo assim, a &ldquo;Teoria At&ocirc;mica&rdquo;.</li></ul>', 5, 1, 'Resumo sobre os filÃ³sofos prÃ©-socrÃ¡ticos'),
(81, 'Surgimento da sociologia', '<p>A Sociologia &eacute; uma ci&ecirc;ncia social que surge em consequ&ecirc;ncia das ideias iluministas e da busca de diferentes intelectuais de compreender a sociedade e suas din&acirc;micas. O principal fator social que conduz ao surgimento deste campo de conhecimento foi a Revolu&ccedil;&atilde;o Industrial e suas consequ&ecirc;ncias, como a divis&atilde;o do trabalho e o processo de urbaniza&ccedil;&atilde;o.</p><p>Autores como <span style=\"background-color: rgb(97, 189, 109);\">Karl Marx, Friedrich Engels, Alexis de Tocqueville, Le Play, Spencer,</span> se dedicaram a estudar os problemas da nova sociedade que surgia na Europa. Em diferentes pa&iacute;ses come&ccedil;ava a aparecer, no s&eacute;culo XIX, o interesse de conhecer os problemas sociais e pol&iacute;ticos a partir de um ponto de vista cient&iacute;fico e todo esse processo indicava o surgimento de um novo campo de conhecimento.</p><p>O nome &ldquo;Sociologia&rdquo; foi criado por <span style=\"color: rgb(0, 0, 0); background-color: rgb(97, 189, 109);\">Auguste Comte</span>, tamb&eacute;m considerado o pai do positivismo. Comte observava uma transforma&ccedil;&atilde;o da sociedade europeia, mais especificamente da sociedade francesa, em que a sociedade militar e teocr&aacute;tica era substitu&iacute;da por uma sociedade industrial e cient&iacute;fica (LALLEMENT, 2008, pg. 71). Essa crise poderia ter consequ&ecirc;ncias catastr&oacute;ficas, e a mem&oacute;ria do que havia sido a Revolu&ccedil;&atilde;o Francesa ainda era muito recente para desejar que outras revoltas acontecessem.</p><p>&Eacute; assim que surge, no s&eacute;culo XIX, a Sociologia, enquanto campo de estudos da sociedade e suas din&acirc;micas. A sociologia francesa &eacute; conhecida como a principal respons&aacute;vel pela institucionaliza&ccedil;&atilde;o da disciplina, quando &Eacute;mile Durkheim foi convidado a ministrar a disciplina em Bord&eacute;us em 1887. Assim sendo, foi primeiramente na Fran&ccedil;a que a disciplina passou a ser considerada uma ci&ecirc;ncia e a fazer parte das disciplinas universit&aacute;rias.</p>', 11, 2, 'Resumo sobre o surgimento da sociologia.'),
(82, 'Geografia do Rio Grande do Norte', '<section><p><span style=\"color: rgb(97, 189, 109);\">Informa&ccedil;&otilde;es sobre a Geografia do Rio Grande do Norte</span></p><p><span style=\"color: rgb(0, 0, 0); background-color: rgb(247, 218, 100);\">Localiza&ccedil;&atilde;o Geogr&aacute;fica:</span> regi&atilde;o Nordeste do Brasil</p><p><span style=\"background-color: rgb(247, 218, 100);\">Limites geogr&aacute;ficos:</span> Oceano Atl&acirc;ntico (norte e leste); Para&iacute;ba (sul e oeste), Cear&aacute; (noroeste)</p><p><span style=\"background-color: rgb(247, 218, 100);\">&Aacute;rea:</span> 52.796,8 km&sup2;</p><p><span style=\"background-color: rgb(247, 218, 100);\">Fronteiras com os seguintes estados</span>: Para&iacute;ba e Cear&aacute;</p><p><span style=\"background-color: rgb(247, 218, 100);\">Clima: </span>tropical na regi&atilde;o litor&acirc;nea e oeste; semi&aacute;rido na central.</p><p><span style=\"background-color: rgb(247, 218, 100);\">Relevo:</span> plan&iacute;cie litor&acirc;nea; planalto no sul; depress&otilde;es em grande parte do restante do territ&oacute;rio.</p><p><span style=\"background-color: rgb(247, 218, 100);\">Vegeta&ccedil;&atilde;o</span>: mangue no litoral; caatinga a oeste; floresta tropical em algumas &aacute;reas pr&oacute;ximas ao litoral.</p><p><span style=\"background-color: rgb(247, 218, 100);\">Ponto mais alto</span>: Serra do Coqueiro (868 metros)</p></section>', 3, 2, 'Resumo sobre a geografia do RN.'),
(85, 'Os PorquÃªs', '<h2>POR QUE</h2><ul><li><p>A forma por que &eacute; a sequ&ecirc;ncia de uma preposi&ccedil;&atilde;o (por) e um pronome interrogativo (que). Equivale a &quot;por qual raz&atilde;o&quot;, &quot;por qual motivo&quot;:</p><p><span style=\"background-color: rgb(41, 105, 176);\">Exemplos:</span></p><ul><li>Desejo saber&nbsp;por que voc&ecirc; voltou t&atilde;o tarde para casa.<br>Por que voc&ecirc; comprou este casaco?<br>H&aacute; casos em que&nbsp;por que&nbsp;representa a sequ&ecirc;ncia&nbsp;preposi&ccedil;&atilde;o + pronome relativo, equivalendo a<em>&nbsp;&quot;pelo qual&quot; (</em>ou alguma de suas flex&otilde;es&nbsp;<em>(pela qual, pelos quais, pelas quais).</em><br>Exemplos:</li><li>Estes s&atilde;o os direitos&nbsp;por que estamos lutando.<br>O t&uacute;nel&nbsp;por que&nbsp;passamos existe h&aacute; muitos anos.</li></ul></li></ul><h2>POR QU&Ecirc;</h2><ul><li><p>Caso surja no final de uma frase, imediatamente antes de um ponto (final, de interroga&ccedil;&atilde;o, de exclama&ccedil;&atilde;o) ou de retic&ecirc;ncias, a sequ&ecirc;ncia deve ser grafada por qu&ecirc;, pois, devido &agrave; posi&ccedil;&atilde;o na frase, o monoss&iacute;labo <em>&quot;que</em>&quot;&nbsp;passa a ser&nbsp;t&ocirc;nico.</p><p><span style=\"background-color: rgb(41, 105, 176);\">Exemplos:</span></p><ul><li>Estudei bastante ontem &agrave; noite. Sabe&nbsp;por qu&ecirc;?<br>Ser&aacute; deselegante se voc&ecirc; perguntar novamente&nbsp;por qu&ecirc;!</li></ul></li></ul><h2>PORQUE</h2><ul><li><p>A forma porque &eacute; uma conjun&ccedil;&atilde;o, equivalendo a <em>pois, j&aacute; que, uma vez que, como.</em>Costuma ser utilizado em respostas, para explica&ccedil;&atilde;o ou causa.</p><p><span style=\"background-color: rgb(61, 142, 185);\">Exemplos:</span></p><ul><li>Vou ao supermercado&nbsp;porque n&atilde;o temos mais frutas.<br>Voc&ecirc; veio at&eacute; aqui&nbsp;porque n&atilde;o conseguiu telefonar?</li></ul></li></ul><h2>PORQU&Ecirc;</h2><ul><li><p>A forma&nbsp;porqu&ecirc;&nbsp;representa um&nbsp;substantivo. Significa &quot;causa&quot;, &quot;raz&atilde;o&quot;, &quot;motivo&quot; e normalmente surge acompanhada de palavra determinante (artigo, por exemplo).</p><p><span style=\"background-color: rgb(61, 142, 185);\">Exemplos:</span></p><ul><li>N&atilde;o consigo entender&nbsp;o porqu&ecirc; de sua aus&ecirc;ncia.<br>Existem muitos&nbsp;porqu&ecirc;s para justificar esta atitude.<br>Voc&ecirc; n&atilde;o vai &agrave; festa? Diga-me ao menos um porqu&ecirc;.</li></ul></li></ul><p><a href=\"http://localhost/helpvestbackup/view/feed.php#!\"></a></p>', 4, 6, 'Resumo sobre os porquÃªs'),
(86, 'CompetÃªncias RedaÃ§Ã£o Enem', '<table height=\"449\" width=\"417\"><tbody><tr><td style=\"background-color: rgb(226, 80, 65);\"><p>Compet&ecirc;ncia 1</p></td><td><p>Demonstrar dom&iacute;nio da modalidade escrita formal da L&iacute;ngua Portuguesa</p></td></tr><tr><td style=\"background-color: rgb(226, 80, 65);\"><p>Compet&ecirc;ncia 2</p></td><td><p>Compreender a proposta de reda&ccedil;&atilde;o e aplicar conceitos das v&aacute;rias &aacute;reas de conhecimento para desenvolver o tema, dentro dos limites estruturais do texto dissertativo-argumentativo em prosa</p></td></tr><tr><td style=\"background-color: rgb(226, 80, 65);\"><p>Compet&ecirc;ncia 3</p></td><td><p>Selecionar, relacionar, organizar e interpretar informa&ccedil;&otilde;es, fatos, opini&otilde;es e argumentos em defesa de um ponto de vista</p></td></tr><tr><td style=\"background-color: rgb(226, 80, 65);\"><p>Compet&ecirc;ncia 4</p></td><td><p>Demonstrar conhecimento dos mecanismos lingu&iacute;sticos necess&aacute;rios para a constru&ccedil;&atilde;o da argumenta&ccedil;&atilde;o</p></td></tr><tr><td style=\"background-color: rgb(226, 80, 65);\"><p>Compet&ecirc;ncia 5</p></td><td><p>Elaborar proposta de interven&ccedil;&atilde;o para o problema abordado, respeitando os direitos humanos</p></td></tr></tbody></table><p><br></p><p><br></p>', 10, 6, 'Resumo sobre as 5 competÃªncias da correÃ§Ã£o da redaÃ§Ã£o do ENEM.'),
(87, 'FunÃ§Ãµes OrgÃ¢nicas', '<p><span style=\"background-color: rgb(0, 168, 133);\">Hidrocarbonetos </span><br>S&atilde;o compostos constitu&iacute;dos por, apenas, &aacute;tomos de carbono e hidrog&ecirc;nio. Sendo essa fun&ccedil;&atilde;o composta por uma ampla gama de combust&iacute;veis (metano, propano, acetileno).</p><p><span style=\"color: rgb(0, 0, 0); background-color: rgb(26, 188, 156);\">Alco&oacute;is </span><br>Os alco&oacute;is s&atilde;o constitu&iacute;dos por radicais de hidrocarbonetos ligados a uma ou mais hidroxilas. Entretanto, nunca podem ser considerados bases de Arrhenius (pois n&atilde;o liberam essa hidroxila em meio aquoso).</p><p><span style=\"background-color: rgb(26, 188, 156);\">Fen&oacute;is </span><br>S&atilde;o cadeias arom&aacute;ticas (hidrocarbonetos) ligados a uma ou mais hidroxilas. Diferindo-se dos alco&oacute;is, portanto, por apresentarem estrutura em an&eacute;is rodeados por grupos OH.</p><p><span style=\"background-color: rgb(0, 168, 133);\">&Eacute;teres </span><br>S&atilde;o compostos por um &aacute;tomo de oxig&ecirc;nio entre duas cadeias carb&ocirc;nicas. Sendo estas cadeias tamb&eacute;m de hidrocarbonetos (radicais alquila ou arila).</p><p><span style=\"background-color: rgb(26, 188, 156);\">&Eacute;steres </span><br>S&atilde;o semelhantes aos &eacute;teres por possu&iacute;rem &aacute;tomos de oxig&ecirc;nio entre as cadeias carb&ocirc;nicas (radicais). Por&eacute;m, diferem-se destes por possu&iacute;rem um grupo carbonilo (CO) tamb&eacute;m entre os carbonos. Assim, a mol&eacute;cula &eacute; estruturada por: radical &ndash; carbonilo &ndash; oxig&ecirc;nio &ndash; radical.</p><p><span style=\"background-color: rgb(26, 188, 156);\">Alde&iacute;dos </span><br>S&atilde;o formados por um radical org&acirc;nico (alif&aacute;tico ou arom&aacute;tico) ligado a um ou mais grupos formilo (HCO).</p><p><span style=\"background-color: rgb(26, 188, 156);\">Cetonas </span><br>S&atilde;o compostas por dois radicais org&acirc;nicos (alif&aacute;ticos ou arom&aacute;ticos) ligados entre si pelo grupo carbonilo (CO). &Eacute; a essa fun&ccedil;&atilde;o que pertence a acetona comercial (propanona &ndash; CH3COCH3).</p><p><span style=\"background-color: rgb(26, 188, 156);\">&Aacute;cidos carbox&iacute;licos </span><br>S&atilde;o radicais alquila, alquenila, arila ou hidrog&ecirc;nio ligados a pelo menos um grupo carbox&iacute;lico (COOH). E, geralmente, s&atilde;o &aacute;cidos fracos (liberam poucos &iacute;ons H+ em meio aquoso).</p><p><span style=\"background-color: rgb(26, 188, 156);\">Aminas </span><br>S&atilde;o compostos nitrogenados onde at&eacute; tr&ecirc;s radicais org&acirc;nicos (arila ou alquila) se ligam a um &aacute;tomo de nitrog&ecirc;niopela substitui&ccedil;&atilde;o de &aacute;tomos de hidrog&ecirc;nio da mol&eacute;cula de am&ocirc;nia. De modo que um radical liga-se ao -NH2, dois radicais a -NH e tr&ecirc;s radicais a -N.</p><p><span style=\"background-color: rgb(26, 188, 156);\">Amidas </span><br>S&atilde;o bem parecidas com as aminas, exceto pela presen&ccedil;a do grupo carbonilo. Assim, at&eacute; tr&ecirc;s radicais acila (RCO) se ligam a um &aacute;tomo de nitrog&ecirc;nio pela substitui&ccedil;&atilde;o de &aacute;tomos de hidrog&ecirc;nio do amon&iacute;aco. Ou seja, as amidas poss&iacute;veis s&atilde;o: RCONH2, (RCO)2NH, e (RCO)3N.</p><p><span style=\"background-color: rgb(26, 188, 156);\">Haletos org&acirc;nicos </span><br>S&atilde;o compostos formados por halog&ecirc;nios (com NOx -1) que substituem &aacute;tomos de hidrog&ecirc;nio pela rea&ccedil;&atilde;o dehalogena&ccedil;&atilde;o. &Eacute; nessa fun&ccedil;&atilde;o org&acirc;nica que se encontram os CFC (clorofluorcarbonetos).</p>', 9, 6, 'Resumo sobre funÃ§Ãµes orgÃ¢nicas.'),
(88, 'Movimento RetilÃ­neo Uniforme', '<p>Dizemos que um objeto est&aacute; se movimento quando este, ao longo do tempo, muda sua posi&ccedil;&atilde;o em rela&ccedil;&atilde;o ao observador. Essa rela&ccedil;&atilde;o de deslocamento e tempo de deslocamento chamamos de velocidade.</p><p>Se, ao longo do tempo, este corpo continua se movendo com a mesma velocidade, falamos que seu movimento &eacute; uniforme. Assim, a cada intervalo igual de tempo, seu deslocamento espacial ser&aacute; o mesmo. Assim, movimento retil&iacute;neo uniforme (MRU) &eacute; descrito como um movimento de um m&oacute;vel em rela&ccedil;&atilde;o a um referencial, movimento este ao longo de uma reta de forma uniforme, ou seja, com velocidade constante.</p><p style=\"text-align: center;\"><span style=\"background-color: rgb(97, 189, 109); font-size: 30px;\" tabindex=\"0\">v=&Delta;S&Delta;t</span></p><p>Sabendo que, para haver o movimento, as duas constantes (varia&ccedil;&atilde;o de espa&ccedil;o e varia&ccedil;&atilde;o de tempo) s&atilde;o diferentes de zero.</p><p>Varia&ccedil;&atilde;o de espa&ccedil;o (&Delta;S): diferen&ccedil;a entre a posi&ccedil;&atilde;o ocupada pelo objeto no instante final (S<sub>f</sub>) de observa&ccedil;&atilde;o e no instante inicial (S<sub>i</sub>).</p><p style=\"text-align: center;\"><span style=\"font-size: 30px; background-color: rgb(97, 189, 109);\" tabindex=\"0\">&Delta;S=Sf&minus;Si</span></p><p>Varia&ccedil;&atilde;o de tempo (&Delta;t): diferen&ccedil;a entre o instante final (t<sub>f</sub>) de observa&ccedil;&atilde;o e no instante inicial (t<sub>i</sub>).</p><p style=\"text-align: center;\"><span style=\"font-size: 30px; background-color: rgb(97, 189, 109);\" tabindex=\"0\">&Delta;t=tf&minus;ti</span></p><p>A velocidade calculada dessa forma &eacute; chama de velocidade m&eacute;dia porque entre o intervalo de tempo usado, a varia&ccedil;&atilde;o do espa&ccedil;o pode ocorrer de formas diferentes do final ou do inicial. Por exemplo, se realizamos uma viagem de 80 km em 1 hora, podemos falar que a velocidade m&eacute;dia nesse intervalo de tempo foi de 80 km/h. Mas sabemos que durante esse trajeto o carro andou em alguns momentos a velocidade maior que esse e com velocidade menor em outros. No entanto, se aproximarmos os instantes final e inicial cada vez mais, maiores s&atilde;o as chances de o espa&ccedil;o sofrer varia&ccedil;&otilde;es cada vez menor. Assim, o &Delta;t fica cada vez menor, cada vez mais pr&oacute;ximo de 0 (mas nunca sendo 0, em absoluto). Teremos ent&atilde;o a velocidade escalar instant&acirc;nea.</p><p style=\"text-align: center;\">Velocidade m&eacute;dia:<br><span style=\"font-size: 30px; background-color: rgb(97, 189, 109);\" tabindex=\"0\">Vm=&Delta;S&Delta;t</span></p><p style=\"text-align: center;\">Velocidade instant&acirc;nea:<br><span style=\"background-color: rgb(97, 189, 109); font-size: 30px;\" tabindex=\"0\">v=lim&Delta;t&rarr;0&Delta;S&Delta;t</span><br>(l&ecirc;-se limite de &Delta;t tendendo a zero)</p><p>Se pegarmos a rela&ccedil;&atilde;o da velocidade:</p><p style=\"text-align: center;\"><span style=\"font-size: 30px; background-color: rgb(97, 189, 109);\" tabindex=\"0\">v=&Delta;S&Delta;t</span></p><p>E a colocarmos em outro formato, levando em conta as varia&ccedil;&otilde;es de espa&ccedil;o e tempo, temos:</p><p style=\"text-align: center;\"><span style=\"font-size: 30px; background-color: rgb(97, 189, 109);\" tabindex=\"0\">v=S&minus;Sit&minus;ti</span></p><p>Considerando t<sub>i</sub>, tempo inicial, como zero:</p><p style=\"text-align: center;\"><span style=\"font-size: 30px; background-color: rgb(97, 189, 109);\" tabindex=\"0\">S=Si+v&sdot;t</span></p><p>Eis a fun&ccedil;&atilde;o hor&aacute;ria do espa&ccedil;o chamada assim pois, sabendo a velocidade e a posi&ccedil;&atilde;o inicial de um corpo, podemos prever sua posi&ccedil;&atilde;o final ao longo do tempo.</p><p>O movimento que um corpo descreve pode ser classificado de acordo com a sua orienta&ccedil;&atilde;o em rela&ccedil;&atilde;o &agrave; trajet&oacute;ria percorrida.</p><p>Movimento progressivo: quando o corpo est&aacute; se movendo no mesmo sentido que a trajet&oacute;ria.</p><p><img src=\"http://localhost/helpvestbackup/view/../imagensHelpvest/f1535d10cf4d81cd8d2431c57bd6c80794c9353c.jpg\" style=\"width: 300px;\" class=\"fr-fic fr-dib\"></p>', 2, 7, 'Resumo sobre MRU.'),
(89, 'Pronombres personales', '<p>S&atilde;o palavras que substituem os substantivos (comuns ou pr&oacute;prios), relacionando-os com as pessoas do discurso.&nbsp;</p><table align=\"center\" border=\"1\" width=\"100%\"><tbody><tr><td style=\"background-color: rgb(84, 172, 210);\" width=\"33%\"><div align=\"center\">Espanhol</div></td><td style=\"background-color: rgb(84, 172, 210);\" width=\"33%\"><div align=\"center\">Portugu&ecirc;s</div></td><td style=\"background-color: rgb(84, 172, 210);\" width=\"33%\"><div align=\"center\"><strong></strong>Pron&uacute;ncia</div></td></tr><tr><td height=\"23\">yo</td><td>eu</td><td>i&oacute;</td></tr><tr><td>t&uacute;</td><td>tu</td><td valign=\"center\" width=\"86\">t&uacute;</td></tr><tr><td valign=\"center\" width=\"94\">&eacute;l</td><td valign=\"center\" width=\"91\">ele</td><td valign=\"center\" width=\"86\">&eacute;l</td></tr><tr><td valign=\"center\" width=\"94\">ella</td><td valign=\"center\" width=\"91\">ela</td><td valign=\"center\" width=\"86\">&eacute;lh&aacute;</td></tr><tr><td valign=\"center\" width=\"94\">nosotros (as)</td><td valign=\"center\" width=\"91\">n&oacute;s</td><td valign=\"center\" width=\"86\">noss&oacute;tros</td></tr><tr><td valign=\"center\" width=\"94\">vosotros (as)</td><td valign=\"center\" width=\"91\">v&oacute;s</td><td valign=\"center\" width=\"86\">boss&oacute;tros</td></tr><tr><td valign=\"center\" width=\"94\">ellos</td><td valign=\"center\" width=\"91\">eles</td><td valign=\"center\" width=\"86\">&eacute;lh&oacute;s</td></tr><tr><td valign=\"center\" width=\"94\">ellas</td><td valign=\"center\" width=\"91\">elas</td><td valign=\"center\" width=\"86\">&eacute;lh&aacute;s</td></tr><tr><td valign=\"center\" width=\"94\">lo</td><td valign=\"center\" width=\"91\">o</td><td valign=\"center\" width=\"86\">l&oacute;</td></tr><tr><td valign=\"center\" width=\"94\">la</td><td valign=\"center\" width=\"91\">a</td><td valign=\"center\" width=\"86\">l&aacute;</td></tr><tr><td valign=\"center\" width=\"94\">los</td><td valign=\"center\" width=\"91\">os</td><td valign=\"center\" width=\"86\">l&oacute;s</td></tr><tr><td valign=\"center\" width=\"94\">las</td><td valign=\"center\" width=\"91\">as</td><td valign=\"center\" width=\"86\">l&aacute;s</td></tr><tr><td valign=\"center\" width=\"94\">le</td><td valign=\"center\" width=\"91\">lhe (a ele)</td><td valign=\"center\" width=\"86\">l&eacute;</td></tr><tr><td valign=\"center\" width=\"94\">le</td><td valign=\"center\" width=\"91\">lhe (a ela)</td><td valign=\"center\" width=\"86\">l&eacute;</td></tr><tr><td valign=\"center\" width=\"94\">les</td><td valign=\"center\" width=\"91\">lhes</td><td valign=\"center\" width=\"86\">l&eacute;s</td></tr></tbody></table>', 6, 7, 'Resumo sobre os pronomes pessoais em espanhol.'),
(90, 'Verbos regulares no passado.', '<h2>VERBOS REGULARES NO PASSADO EM INGL&Ecirc;S</h2><p>Para a maioria dos chamados verbos regulares, voc&ecirc; s&oacute; precisa acrescentar &ldquo;-ed&rdquo;, como em:<br>Work &ndash; work<span style=\"background-color: rgb(41, 105, 176);\">ed</span><br>Walk &ndash; walk<span style=\"background-color: rgb(41, 105, 176);\">ed</span><br>Wait &ndash; wait<span style=\"background-color: rgb(41, 105, 176);\">ed</span></p><p>Com verbos regulares terminados em&nbsp;e, basta acrescentar o &ldquo;d&rdquo;:<br>Arrive &ndash; arrive<span style=\"background-color: rgb(41, 105, 176);\">d</span><br>Like &ndash; like<span style=\"background-color: rgb(41, 105, 176);\">d</span><br>Love &ndash; love<span style=\"background-color: rgb(41, 105, 176);\">d</span></p><p>Agora, com verbos terminados em consoante + vogal + consoante, deve-se&nbsp;dobrar a &uacute;ltima consoante e acrescentar o &ldquo;-ed&rdquo;. Veja:<br>Plan &ndash; plan<span style=\"background-color: rgb(41, 105, 176);\">ned</span><br>Stop &ndash; stop<span style=\"background-color: rgb(41, 105, 176);\">ped</span><br>Prefer &ndash; prefer<span style=\"background-color: rgb(41, 105, 176);\">red</span></p>', 12, 7, 'Resumo sobre verbos regulares no passado em inglÃªs.'),
(94, 'Titulo teste', '<p><span style=\"background-color: rgb(0, 168, 133);\">agora quero outra cor.</span></p>', 2, 12, 'resumo para banca'),
(95, 'resumo teste de matematica', '<p>teste</p>', 1, 2, 'resumo teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `questionario`
--

CREATE TABLE `questionario` (
  `idquest` int(11) NOT NULL,
  `idresumo` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idrespcerta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_user` int(11) NOT NULL,
  `nome_user` varchar(50) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `senha_user` varchar(300) NOT NULL,
  `nome_imagem` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_user`, `nome_user`, `email_user`, `senha_user`, `nome_imagem`) VALUES
(1, 'Gabriel Viegas da Silva', 'gabrielgl13@hotmail.com', '$2y$10$J7CHDdxMvCCkSBeLu05MG.OGSgapj9ZQdEnN5h4zC8U8GngBiWqhe', 'IMG_20180612_110948440_BURST000_COVER_TOP1.jpg'),
(2, 'Emilly', 'emillyzoldan@gmail.com', '$2y$10$2C0/c3cZihcc8OAxm7PNi.fqAvob4yYQprawzUVmgxiiqq/vrQEnu', 'emy22.jpg'),
(3, 'Daniel', 'danielzoldan@gmail.com', '$2y$10$UiLb/J7kxp9iBSFRoWz53eigeSghgSKe37AXvahqFAU8V9cS85V2y', ''),
(5, 'Emannuelli', 'princesamannukinha@gmail.com', '$2y$10$0Q5h48BVGwwc5Yl7nmoPO.5heWBmPQf1XaofN6H0caToiyb.df2zy', ''),
(6, 'Luis Felipe', 'luisfelipe@gmail.com', '$2y$10$b2yLYM4SRAl/X.aJ/N1Yk.PSJK6ChCR94rA7wdCsjWMj9UtFJf20.', 'luis6.jpg'),
(7, 'VitÃ³ria Quintana', 'vitoriaquintana@gmail.com', '$2y$10$nyKEj6kxP045WYaG9PB1vuESUrk0tFD/.zP4BbT3REWwZazhGlO8a', 'vitoria7.jpg'),
(13, 'Emannuelli', 'emannuelli@gmail.com', '$2y$10$sdxxypLmQF4zb80Cdsr0m.LmPSb2wqXjiHTmYPwMg8hRJAy2GHuXi', 'mannu13.jpg'),
(14, 'Teste', 'teste@teste.com', '$2y$10$3KTXqiD88ULGo864dXUIE.NUWt463YWsismLXLdLriIulAl/7P8Lu', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alternativas`
--
ALTER TABLE `alternativas`
  ADD PRIMARY KEY (`idalternativa`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `pasta`
--
ALTER TABLE `pasta`
  ADD PRIMARY KEY (`idpasta`);

--
-- Indexes for table `pergunta`
--
ALTER TABLE `pergunta`
  ADD PRIMARY KEY (`idpergunta`);

--
-- Indexes for table `postagem`
--
ALTER TABLE `postagem`
  ADD PRIMARY KEY (`id_postagem`);

--
-- Indexes for table `questionario`
--
ALTER TABLE `questionario`
  ADD PRIMARY KEY (`idquest`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alternativas`
--
ALTER TABLE `alternativas`
  MODIFY `idalternativa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pasta`
--
ALTER TABLE `pasta`
  MODIFY `idpasta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pergunta`
--
ALTER TABLE `pergunta`
  MODIFY `idpergunta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `postagem`
--
ALTER TABLE `postagem`
  MODIFY `id_postagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `questionario`
--
ALTER TABLE `questionario`
  MODIFY `idquest` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
