<html>

<head>
    <title>Login e Cadastro</title>
    <meta charset="UTF-8" />
    <!-- CSS -->
    <link rel="stylesheet" href="../assets/css/login.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
</head>

<body class="bg-image">
    <div class="container mt-default">

        <!-- Card -->
        <div class="row">
            <div class="col s12 m5 offset-m3">
                <div class="card">
                    <div class="header center-align">
                        <h5 class="card-title">Faça seu login</h5>
                        <img src="../assets/images/user.png" class="circle img-user">
                    </div>
                    <div class="card-content">
                        <form method="POST" action="../controller/UsuarioController.php">
                            <input type="hidden" name="autenticar" value="autenticar">
                            <div class="input-field">
                                <input type="email" name="emailLogin"  required>
                                <label for="email">Endereço de e-mail</label>
                            </div>
                            <div class="input-field">
                                <input type="password" name="senhaLogin" id="senha" required>
                                <label for="senha">Senha</label>
                            </div>
                            <button type="submit" class="waves-effect waves-light btn button-customized">
                                Entrar
                            </button>
                        </form>

                        <a href="#modal-register" class="waves-effect waves-light btn modal-trigger button-customized">
                            Registre-se
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="modal-register" class="modal">
            <div class="modal-content">
                <div class="row">
                    <div class="col s12">
                        <h4 class="center-align">Cadastre-se</h4>
                        <span class="modal-close" style="position: absolute; right: 20px; top: 10px;">
                            <i class="material-icons">close</i>
                        </span>
                        <form method="POST" action="../controller/UsuarioController.php">
                            <input type="hidden" name="cadastrar" value="cadastrar" />
                            <div class="input-field">
                                <input type="text" name="nome" id="nome" required>
                                <label for="nome">Nome</label>
                            </div>
                            <div class="input-field">
                                <input type="email" name="email" id="email" required>
                                <label for="email">E-mail</label>
                            </div>
                            <div class="input-field">
                                <input type="password" name="senha" required>
                                <label for="senha">Senha</label>
                            </div>
                            <div class="input-field">
                                <input type="password" name="confirmarSenha" id="confirmarSenha" required>
                                <label for="confirmarSenha">Confirmar Senha</label>
                            </div>
                            <button type="submit" class="waves-effect waves-light btn button-customized">
                                Cadastrar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Javascript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="../assets/js/modal.js" type="text/javascript"></script>
</body>

</html>