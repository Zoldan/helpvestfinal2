<?php

if (!session_id()) {
    session_start();
}

if (!isset($_SESSION['usuario'])) {
    header("Location: ../view/login.php");
}

require_once '../controller/QuestionarioController.php';
require_once '../controller/PostagemController.php';

?>

<html>

<head>
    <meta charset="UTF-8">
   <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="../assets/css/create.css">
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--  <script src="../assets/js/main.js"></script> -->
    <title>Meus Posts</title>
</head>

<body class="bg-image">

    <!-- Menu Horizontal do Topo -->
    <nav>
        <div class="nav-wrapper blue-grey">
            <div class="container">
                <a href="feed.php" class="brand-logo black-text">Helpvest</a>
            </div>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="create.php" class="black-text">Enviar Resumo</a></li>
                <li><a href="visualizarSeusPosts.php" class="black-text">Meus Resumos</a></li>
                <li><a href="pastasView.php" class="black-text">Minhas Pastas</a></li>
            </ul>
        </div>
    </nav>

    <h3 class="center-align">Meus Resumos</h3>

    <div class="container">
        <div class="row">
            <div class="col s12 card-panel">
                <table class="striped">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Titulo</th>
                            <th scope="col">Categoria</th>
                            <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $posts = PostagemController::getAll();
                        if (!empty($posts)) {
                            foreach ($posts as $value) {
                                if ($value[4] == $_SESSION["usuario"][0]) {
                                    ?>
                                    <tr>
                                        <th scope="row"><?= $value[0]; ?></th>
                                        <td><?= $value[1]; ?></td>
                                        <td><?= utf8_encode(PostagemController::buscarCategoriaPorPost($value[3])); ?></td>
                                        <td>
                                            <!-- Parte de remover -->
                                            <a class="modal-trigger" href="#modalRemove<?php echo $value[0]; ?>" title="Excluir resumo">
                                                <i style="color: black;" class="material-icons prefix">delete</i>
                                            </a>

                                            <!-- Parte de visualizar -->
                                            <a class="modal-trigger" href="#modalVisualizar<?php echo $value[0]; ?>" title="Visualizar resumo">
                                                <i style="color: black;" class="material-icons prefix">remove_red_eye</i>
                                            </a>

                                            <!-- Parte de editar -->
                                            <a href="editarPostagem.php?id=<?= $value[0]; ?>" title="Editar resumo">
                                                <i style="color: black;" class="material-icons prefix">edit</i>
                                            </a>

                                            <!-- Parte de questionário -->
                                            <a class="modal-trigger" href="#modalQuestionario?id=<?= $value[0]; ?>" title="Criar questionário">
                                                <i style="color: black;" class="material-icons prefix">description</i>
                                            </a>
                                        </td>
                                    </tr>

                                    <!-- Modal de criação de questionário -->

                                    <div id="modalQuestionario?id=<?= $value[0]; ?>" class="modal">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col s-12">
                                                    <h3 class="center-align">
                                                        Criação de Questionário
                                                    </h3>
                                                </div>

                                                <?php 
                                                    if (QuestionarioController::verificarExistencia($value[0])) {
                                                        $questionario = QuestionarioController::mostrarQuestionario($value[0]);
                                                        $pergunta = QuestionarioController::mostrarPergunta($questionario[0]);
                                                        $alternativas = QuestionarioController::mostrarAlternativas($pergunta[0]);
                                                ?>
                                                    <form method="POST" action="../controller/QuestionarioController.php" id="formQuestEdit">
                                                        <input type="hidden" name="idQuestionario" value="<?= $questionario[0]; ?>">
                                                        <input type="hidden" name="idPergunta" value="<?= $pergunta[0]; ?>">
                                                        <div class="row">
                                                            <div class="col s4">
                                                                <div class="input-field col s12">
                                                                    Alternativa 1
                                                                    <input type="hidden" name="idAlternativa1" value="<?= $alternativas[0][0]; ?>">
                                                                    <textarea name="alternativa[]">
                                                                        <?= $alternativas[0][2]; ?>
                                                                    </textarea>
                                                                </div>
                                                    
                                                                <div class="input-field col s12">
                                                                    Alternativa 2
                                                                    <input type="hidden" name="idAlternativa2" value="<?= $alternativas[1][0]; ?>">
                                                                    <textarea name="alternativa[]">
                                                                        <?= $alternativas[1][2];?>
                                                                    </textarea>
                                                                </div>

                                                                <div class="input-field col s12">
                                                                    Alternativa 3
                                                                    <input type="hidden" name="idAlternativa3" value="<?= $alternativas[2][0]; ?>">
                                                                    <textarea name="alternativa[]">
                                                                        <?= $alternativas[2][2]; ?>
                                                                    </textarea>
                                                                </div>

                                                                <div class="input-field col s12">
                                                                    Alternativa 4 
                                                                    <input type="hidden" name="idAlternativa4" value="<?= $alternativas[3][0]; ?>">
                                                                    <textarea name="alternativa[]">
                                                                        <?= $alternativas[3][2]; ?>
                                                                    </textarea>
                                                                </div>

                                                                <div class="col s12">
                                                                    <label>Informe a resposta certa abaixo:</label>
                                                                    <select name="opcao_correta" class="browser-default">
                                                                        <option value="0" <?= $questionario[3] == 0 ? "selected" : ""; ?>>1</option>
                                                                        <option value="1" <?= $questionario[3] == 1 ? "selected" : ""; ?>>2</option>
                                                                        <option value="2" <?= $questionario[3] == 2 ? "selected" : ""; ?>>3</option>
                                                                        <option value="3" <?= $questionario[3] == 3 ? "selected" : ""; ?>>4</option>
                                                                    </select>
                                                                </div>

                                                            </div>

                                                            <div class="col s7">
                                                                <input type="hidden" name="edita_questionario" value="edita_questionario">
                                                                <input type="hidden" name="idPostagem" value="<?= $value[0]; ?>">
                                                                <input type="hidden" name="idUser" value="<?= $value[4]; ?>">
                                                                Pergunta
                                                                <textarea name="pergunta" style="height: 300px;">
                                                                    <?= $pergunta[2]; ?>
                                                                </textarea>
                                                                <button onclick="swal('Editado!').then((value) => {document.getElementById ('formQuestEdit').submit()}); return false;" type="submit" name="action">Enviar</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                <?php
                                                    } else {
                                                ?>
                                                    <form id="formQuestCreate" method="POST" action="../controller/QuestionarioController.php">
                                                        <div class="row">
                                                            <div class="col s4">
                                                                <div class="input-field col s12">
                                                                    Alternativa 1
                                                                    <textarea name="alternativa[]"></textarea>
                                                                </div>
                                                    
                                                                <div class="input-field col s12">
                                                                    Alternativa 2
                                                                    <textarea name="alternativa[]"></textarea>
                                                                </div>

                                                                <div class="input-field col s12">
                                                                    Alternativa 3
                                                                    <textarea name="alternativa[]"></textarea>
                                                                </div>

                                                                <div class="input-field col s12">
                                                                    Alternativa 4 
                                                                    <textarea name="alternativa[]"></textarea>
                                                                </div>

                                                                <div class="col s12">
                                                                    <label>Informe a resposta certa abaixo:</label>
                                                                    <select name="opcao_correta" class="browser-default">
                                                                        <option value="" disabled selected>Selecione uma</option>
                                                                        <option value="0">1</option>
                                                                        <option value="1">2</option>
                                                                        <option value="2">3</option>
                                                                        <option value="3">4</option>
                                                                    </select>
                                                                </div>

                                                            </div>

                                                            <div class="col s7">
                                                                <input type="hidden" name="cria_questionario" value="cria_questionario">
                                                                <input type="hidden" name="idPostagem" value="<?= $value[0]; ?>">
                                                                <input type="hidden" name="idUser" value="<?= $value[4]; ?>">
                                                                Pergunta
                                                                <textarea name="pergunta" style="height: 300px;"></textarea>
                                                                <button onclick="swal('Cadastrado com sucesso!').then((value) => {document.getElementById ('formQuestCreate').submit()}); return false;" type="submit">Enviar</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php 
                                            if (QuestionarioController::verificarExistencia($value[0])) {
                                        ?>
                                            <a class="modal-trigger btn red" style="width: 100%; margin-bottom: 20px;" href="#modalRemoveQuestionario<?= $value[0]; ?>" title="Excluir Questionário">
                                                Excluir Questionário
                                            </a>
                                        <?php } ?>
                                    </div>

                                    <!-- Modal remove questionário -->
                                    <div id="modalRemoveQuestionario<?= $value[0]; ?>" class="modal">
                                        <div class="modal-content">
                                            <p>
                                                O questionário selecionado será removido, você tem certeza que deseja deletá-lo?
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <form id="formApagaQuest" method="POST" action="../controller/QuestionarioController.php">
                                                <input type="hidden" name="delete" value="delete">
                                                <input type="hidden" name="idAlternativa[]" value="<?= $alternativas[0][0]; ?>">
                                                <input type="hidden" name="idAlternativa[]" value="<?= $alternativas[1][0]; ?>">
                                                <input type="hidden" name="idAlternativa[]" value="<?= $alternativas[2][0]; ?>">
                                                <input type="hidden" name="idAlternativa[]" value="<?= $alternativas[3][0]; ?>">
                                                <input type="hidden" name="idQuestionario" value="<?= $questionario[0]; ?>">
                                                <input type="hidden" name="idPergunta" value="<?= $pergunta[0]; ?>">
                                                <button onclick="swal('Excluído com sucesso!').then((value) => {document.getElementById ('formApagaQuest').submit()}); return false;" id="removeQuest" class="modal-close waves-effect btn waves-light" type="submit" name="action">Sim
                                                </button>
                                            </form>
                                            <a class="modal-close waves-effect btn waves-light">Não</a>
                                        </div>
                                    </div>

                                    <!-- Modal de visualização -->
                                    <div id="modalVisualizar<?php echo $value[0]; ?>" class="modal" style=" width: 85% !important; height: 100% !important; max-height: 80%;">
                                        <div class="row">
                                            <div class="col s7">
                                                <div class="modal-content align-center">
                                                    <p><b>Título: "<?= $value[1] ?>"</b></p>
                                                </div>
                                                <div class="modal-footer align-center">
                                                    <?= $value[2]; ?>
                                                </div>
                                            </div>
                                            <div class="col s5">
                                                <div class="row teal lighten-1">
                                                    <div class="card-panel">
                                                        <p class="align-center">
                                                            <b> Autor </b>
                                                        </p>
                                                        <div class="image-container center">
                                                            <img src="../upload/<?= PostagemController::retornaUser($value[0])[0][1]; ?>" class="circle responsive-img" style="max-width: 100px; max-height: 100px;">
                                                        </div>
                                                        <p class="align-center"><b>
                                                                <?php echo PostagemController::retornaUser($value[0])[0][0]; ?></b>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Modal de remoção -->
                                    <div id="modalRemove<?php echo $value[0]; ?>" class="modal">
                                        <div class="modal-content">
                                            <p>
                                                O item <b>"<?= $value[1]; ?>"</b> será removido, você tem certeza que deseja remover?
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <form method="POST" action="../controller/PostagemController.php">
                                                <input type="hidden" name="delete" value="<?= $value[0]; ?>">
                                                <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action">Sim
                                                </button>
                                            </form>
                                            <a class="modal-close waves-effect waves-light btn-small red">Não</a>
                                        </div>
                                    </div>
                        <?php
                                }
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Javascript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="../assets/js/modal.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>