<?php

if (!session_id()) {
    session_start();
}

if(!isset($_SESSION['usuario'])){
    header("Location: ../view/login.php");
}

?>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <link rel="stylesheet" href="../assets/css/create.css">
        <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Resumo</title>
    </head>
    <script src="https://cdn.tiny.cloud/1/tftq6c29ypcjjwz367z2v0lpnp0j4jyivdhadrk7f1sqfsk9/tinymce/5/tinymce.min.js"></script>
<script src="tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: '#conteudo',
    plugins: 'image code',
    toolbar: 'undo redo | image code',
    
    // without images_upload_url set, Upload tab won't show up
    images_upload_url: 'uploadTiny.php',
    
    // override default upload handler to simulate successful upload
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
      
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'uploadTiny.php');
      
        xhr.onload = function() {
            var json;
        
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
        
            json = JSON.parse(xhr.responseText);
        
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
        
            success(json.location);
        };
      
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
      
        xhr.send(formData);
    },
});
</script>
    <body class="bg-image">

        <!-- Menu Horizontal do Topo -->
        <nav id="x">
            <div class="nav-wrapper blue-grey">
                <div class="container">
                    <a href="feed.php" class="brand-logo black-text">Helpvest</a>
                </div>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="create.php" class="black-text">Enviar Resumo</a></li>
                    <li><a href="visualizarSeusPosts.php" class="black-text">Meus Resumos</a></li>
                    <li><a href="pastasView.php" class="black-text">Minhas Pastas</a></li>
                </ul>
            </div>
        </nav>
    
        <h2 class="center-align">Escreva seu Resumo</h2>

        <!-- FORMULÁRIO DE ENVIO DE RESUMO -->
        <div class="container">
            <div class="row">
                <div class="col s10 offset-s1 card-panel">
                    <form method="POST" action="../controller/PostagemController.php" id="formCadastro">
                        <input type="hidden" name="insert" value="insert"/>
                        <div class="input-field">
                            <i class="material-icons prefix">title</i>
                            <input id="tituloResumo" type="text" name="titulo" class="validate" required maxlength="35">
                            <label for="tituloResumo">Título</label>
                        </div>
                        <div class="input-field">
                            <i class="material-icons prefix">info</i>
                            <textarea style="height: 300px;" rows="4" cols="5" id="conteudo" name="conteudo" class="materialize-textarea"></textarea>
                        </div>

                        <div class="row">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">info</i>
                                <textarea rows="4" cols="5" id="breveDescricao" name="breveDescricao" class="materialize-textarea"></textarea>
                                <label for="breveDescricao">Breve Descrição</label>
                            </div>
                            <div class="input-field col s6">
                                <select name="categoria">
                                    <option value="" disabled selected>Escolha a categoria do resumo</option>
                                    <option value="1">Matemática</option>
                                    <option value="7">Biologia</option>
                                    <option value="2">Física</option>
                                    <option value="8">História</option>
                                    <option value="3">Geografia</option>
                                    <option value="9">Química</option>
                                    <option value="4">Português</option>
                                    <option value="10">Redação</option>
                                    <option value="5">Filosofia</option>
                                    <option value="11">Sociologia</option>
                                    <option value="6">Espanhol</option>
                                    <option value="12">Inglês</option>
                                </select>
                                <label>Categorias</label>
                            </div>
                            <button onclick="swal('Cadastrado com sucesso!').then((value) => {formCadastro.submit()}); return false;" id="cadastrarResumo" class="btn waves-light col s12" type="submit" name="action">Cadastrar
                            </button>
                           
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Javascript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <script src="../assets/js/main.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/js/froala_editor.pkgd.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    </body>
</html>
