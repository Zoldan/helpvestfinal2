<?php

if (!session_id()) {
    session_start();
}

if (!isset($_SESSION['usuario'])) {
    header("Location: ../view/login.php");
}

require_once '../controller/PostagemController.php';
require_once '../controller/PastaController.php';
require_once '../controller/QuestionarioController.php';

?>

<html>

<head>
    <meta charset="UTF-8">
    <title> Página Inicial </title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="../assets/css/feed.css">
    <link href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="../assets/js/sweetAlerts.js"></script>


<body>
    <main>
        <!-- Menu Horizontal topo -->
        <nav id="t">
            <div class="nav-wrapper blue-grey">
                <div class="container">
                    <a href="feed.php" class="brand-logo black-text">Helpvest</a>
                </div>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li>
                        <div class="input-field black-text">
                        <form method="post" action="">
                        <?php
                        if (isset($_GET['pagina'])){ ?>
                            <input type="hidden" name="pagina" value="<?= $_GET['pagina']; ?>">
                            <?php
                        } else {
                            ?>
                            <input type="hidden" name="pagina" value="1">
                            <?php
                        }
                        ?>
                        
                        <div style="float: left;" id="searching">
                            <i class="black-text material-icons prefix">search</i>
                            <input type="text" name="pesquisar" class="black-text">
                            </div>
                            <button style="margin-top: 10px;" class="btn waves-effect waves-light" type="submit">Pesquisar</button>
                            </form>
                        </div>
                    </li>
                    <li><a href="create.php" class="black-text">Enviar Resumo</a></li>
                    <li><a href="visualizarSeusPosts.php" class="black-text">Meus Resumos</a></li>
                    <li><a href="pastasView.php" class="black-text">Minhas Pastas</a></li>
                </ul>
            </div>
        </nav>

        <!-- Aqui puxa as postagens do banco de dados -->
        <?php
        if (isset($_GET['pagina'])) {
            $active = $_GET['pagina'];
            
            if (isset($_GET['idcategoria'])){
                $posts = PostagemController::pagination($_GET['pagina'], $_GET['idcategoria']);
            } else {
                $posts = PostagemController::pagination($_GET['pagina']);
            }
            
        } else {

            if (isset($_GET['idcategoria'])){
                $posts = PostagemController::pagination(1, $_GET['idcategoria']);
            } else {
                $posts = PostagemController::pagination(1);
            }                
            $active = 1;
        }
        ?>

        <!-- Menu Lateral -->
        <div class="row">
            <div class="col s2">
                <div id="menuLateral" class="row">
                    <ul class="collection side-nav fixed user-details teal lighteen-2" style="border: 0px">
                        <!-- Meu Perfil -->
                        <li class="collection-item avatar" style="background-color: #009688;">
                            <?php if (empty($_SESSION["usuario"][4])) {
                                ?>
                                <img id="imagemPerfil" src="../assets/images/user.png" class="circle modal-trigger" href="#mudarImagem">
                            <?php
                            } else { ?>
                                <img id="imagemPerfil" src="../upload/<?= $_SESSION["usuario"][4]; ?>" class="circle modal-trigger" href="#mudarImagem">
                            <?php
                            } ?>
                            <?php
                            $nomeUser = $_SESSION["usuario"][1];
                            $nome[] = explode(" ", $nomeUser);
                            ?>
                            <span id="nomePerfil" class="title"><?php echo $nome[0][0]; ?></span>
                        </li>
                        <!-- Listagem de Disciplinas -->
                        <li><a class="black-text" href="?idcategoria=1">Matemática</a></li>
                        <li><a class="black-text" href="?idcategoria=7">Biologia</a></li>
                        <li><a class="black-text" href="?idcategoria=5">Filosofia</a></li>
                        <li><a class="black-text" href="?idcategoria=11">Sociologia</a></li>
                        <li><a class="black-text" href="?idcategoria=3">Geografia</a></li>
                        <li><a class="black-text" href="?idcategoria=8">História</a></li>
                        <li><a class="black-text" href="?idcategoria=4">Português</a></li>
                        <li><a class="black-text" href="?idcategoria=10">Redação</a></li>
                        <li><a class="black-text" href="?idcategoria=9">Química</a></li>
                        <li><a class="black-text" href="?idcategoria=2">Física</a></li>
                        <li><a class="black-text" href="?idcategoria=6">Espanhol</a></li>
                        <li><a class="black-text" href="?idcategoria=12">Inglês</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Modal para mudar foto de perfil -->
        <div style="height: 230px;" id="mudarImagem" class="modal">
            <div class="modal-content">
                 <b>Alterar Foto de Perfil</b> </p>
            </div>
            <div class="modal-footer">
                <form id="formFoto" method="post" action="../controller/UsuarioController.php" enctype="multipart/form-data">
                    <input type="hidden" name="alterarImagem" value="alterarImagem">
                    <input type="hidden" name="idUser" value="<?= $_SESSION["usuario"][0] ?>">
                    <input type="hidden" name="MAX_FILE_SIZE" value="10485760">
                    <div class="file-field input-field">
                        <div class="btn">
                            <span>Arquivo</span>
                            <input type="file" name="arquivo">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                    <div>
                    <button onclick="swal('Foto salva').then((value) => {formFoto.submit()}); return false;" id="salvarAlteracoes" class="btn waves-light col s12" type="submit" name="action">Salvar Alterações
                    </button>
                           </form>
                <a class="modal-close btn waves-light col s12">Fechar</a>
                </div>
            </div>
        </div>

        <!-- Exibição das Cartas -->
        <div class="row">
            <div class="col s10 offset-s2" id="postagens">
                <?php
                $count = 0;
                if (!empty($posts["items"])) {
                    foreach ($posts["items"] as $value) {
                        $count++;
                        if ($count == 1) { ?>
                            <div class="row">
                            <?php
                                    }
                                    ?>
                            <div class="carta" value="<?= $value->id_categoria; ?>">
                                <div class="col s10 m4">
                                    <div class="card blue-grey darken-1">
                                        <div class="card-content white-text">
                                            <span class="card-title"><?= $value->titulo ?></span>
                                            <p><?= $value->breve_descricao ?></p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">Escrito por: <?= PostagemController::retornaUser($value->id_postagem)[0][0]; ?></a>
                                            <a class="modal-trigger aciona" name="<?= $value->id_postagem; ?>" href="#modalLerMais<?= $value->id_postagem; ?>">Ler mais</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal Carta -->
                            <div id="modalLerMais<?= $value->id_postagem; ?>" class="modal" style=" width: 85% !important; height: 100% !important; max-height: 80%;  ">
                                <div class="row">
                                    <div class="col s7 m7 l7">
                                        <div class="modal-content center-align">
                                            <p><b>Título: "<?= $value->titulo; ?>"</b></p>
                                        </div>
                                        <div class="modal-footer center-align">
                                            <?= $value->conteudo; ?>
                                        </div>
                                    </div>
                                    <div class="col s5">
                                        <div class="container" id="autor">
                                            <div class="row teal lighten-1">
                                                <div class="card-panel">
                                                    <p class="center-align"><b> Autor </b></p>
                                                    <div class="image-container center">
                                                        <img src="../upload/<?= PostagemController::retornaUser($value->id_postagem)[0][1]; ?>" class="circle responsive-img" style="max-width: 100px; max-height: 100px;">
                                                    </div>
                                                    <p class="center-align"><b>
                                                            <?= PostagemController::retornaUser($value->id_postagem)[0][0]; ?></b>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="TO TENTANDO center-align">
                                        <div id="interacoes">
                                            <div class="curtir">
                                           <h3>
                                           <div class="qtd"> 
                                           <?php echo PostagemController::qtdCurtidas($value->id_postagem); ?> 
                                           </div>
                                           </h3>
                                           <?php
                                           //print_r($value->id_postagem . $_SESSION["usuario"][0]);
                                           if (!PostagemController::possuiCurtida($value->id_postagem, $_SESSION["usuario"][0])){
                                               ?>
                                            <a href="#" class="waves-effect waves-green btn-flat">Curtir</a>
                                            <?php
                                           } else {
                                               ?>
                                               <a href="#" class="waves-effect waves-green btn-flat">Descurtir</a>
                                               <?php
                                           }
                                           ?>
                                            </div>
                                            
                                            <?php
                                                    if (QuestionarioController::verificarExistencia($value->id_postagem)) {
                                                        ?>
                                                <a class="modal-trigger" href="#modalQuestionario<?= $value->id_postagem; ?>">RESPONDER QUESTIONÁRIO</a>
                                            <?php
                                                    }
                                                    ?>
                                            <?php
                                                    $status = PastaController::verificarStatus($_SESSION["usuario"][0], $value->id_postagem, $value->id_user);
                                                    if ($status == 2) {
                                                        ?>
                                                <form method="POST" action="../controller/PastaController.php">
                                                    <input type="hidden" value="adicionaPasta" name="adicionaPasta">
                                                    <input type="hidden" name="idPost" value="<?= $value->id_postagem ?>">
                                                    <input type="hidden" name="idCategoria" value="<?= $value->id_categoria ?>">
                                                    <input type="hidden" name="idUser" value="<?= $_SESSION["usuario"][0]; ?>">
                                                    <button type="submit" class="waves-effect waves-green btn-flat">Adicionar a pasta</button>
                                                </form>
                                            <?php
                                                    }
                                                    ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <!-- Modal Carta -->
                            <div id="modalQuestionario<?= $value->id_postagem; ?>" class="modal">
                                <div class="row">
                                    <div class="col s12">
                                        <h1 class="center-align">
                                            Questionário
                                        </h1>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <?php
                                            $questionario = QuestionarioController::mostrarQuestionario($value->id_postagem);
                                            $pergunta = QuestionarioController::mostrarPergunta($questionario[0]);
                                            $alternativas = QuestionarioController::mostrarAlternativas($pergunta[0]);
                                        ?>
                                            <h5 class="center-align"><?= $pergunta[2]; ?></h5>
                                        <?php
                                            foreach ($alternativas as $key => $alternativa) {
                                                if ($key == $questionario[3]) {
                                                    
                                                    ?>
                                                    <p name="correta">
                                                        <input type="radio" class="group" name="group" id="<?= 'alternativa' . $alternativa[0];?>" />
                                                        <label for="<?= 'alternativa' . $alternativa[0];?>"><?= $alternativa[2];?></label>
                                                    </p>
                                                <?php
                                                } else {
                                                   
                                        ?>
                                                    <p name="errada">
                                                        <input type="radio" class="group" name="group" id="<?= 'alternativa' . $alternativa[0];?>" />
                                                        <label for="<?= 'alternativa' . $alternativa[0];?>"><?= $alternativa[2];?></label>
                                                    </p>
                                        <?php
                                                }
                                        
                                            }
                                        ?>
                                        <button onclick="" class="waves-effect waves-light btn right" id="selecionarAlternativa"> Marcar </button>
                                            
                                    </div>
                                </div>
                            </div>

                    <?php
                            if ($count == 3) {
                                $count = 1;
                            }
                        }
                    }
                    ?>
                            </div>

                            <!-- Paginação -->
                            <div id="paginacao">
                                <ul class="pagination center">
                                    <?php if ($active == 1) { ?>
                                        <li class="disabled"><a href="#">
                                                <i class="material-icons">chevron_left</i></a>
                                        </li>
                                    <?php } else { ?>
                                        <li>
                                            <a href="feed.php?pesquisar=<?=@$_REQUEST['pesquisar'];?>&idcategoria=<?=@$_GET['idcategoria'];?>&pagina=<?= $active - 1; ?>">
                                                <i class="material-icons">chevron_left</i>
                                            </a>
                                        </li>
                                        <?php }

                                        for ($x = 0; $x < $posts['count']; $x++) {
                                            if ($x + 1 == $active) {
                                                ?>
                                            <li class="waves-effect active">
                                                <a href="feed.php?pesquisar=<?=@$_REQUEST['pesquisar'];?>&idcategoria=<?=@$_GET['idcategoria'];?>&pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                                        <?php
                                            } else {
                                                ?>
                                            <li class="waves-effect">
                                                <a href="feed.php?pesquisar=<?=@$_REQUEST['pesquisar'];?>&idcategoria=<?=@$_GET['idcategoria'];?>&pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a>
                                            </li>
                                        <?php
                                            }
                                        }

                                        if ($active == $posts['count']) { ?>
                                        <li class="waves-effect disabled">
                                            <a href="#">
                                                <i class="material-icons">chevron_right</i>
                                            </a>
                                        </li>
                                    <?php } else { ?>
                                        <li class="waves-effect">
                                            <a href="feed.php?pesquisar=<?=@$_REQUEST['pesquisar'];?>&idcategoria=<?=@$_GET['idcategoria'];?>&pagina=<?= $active + 1; ?>">
                                                <i class="material-icons">chevron_right</i>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
            </div>
        </div>

        <!-- Rodapé -->
        <footer id="rodape" class="page-footer blue-grey">
            <div class="container blue-grey">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="black-text" style="margin-left: 80px;">HELPVEST</h5>
                        <h6 style="text-align: center">Sua plataforma de estudos online!</h6>
                        <h6 style="text-align: center"> Sair da plataforma</h6>
                    </div>
                </div>
            </div>
            <div style="text-align: right; margin-right: 50px; margin-top: 20px;">
                <a href="../controller/UsuarioController.php?message=sair" style="color: white">
                    Sair da plataforma
                </a>
            </div>
        </footer>
    </main>

    <!-- JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="../assets/js/main.js" type="text/javascript"></script>
  <!--  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
     <script scr="../assets/js/sweetAlerts.js" type="text/javascript"></script>
     <script src="../assets/js/curtir.js" type="text/javascript"></script>


</body>

</html>