<!DOCTYPE html>

<?php

if (!session_id()) {
    session_start();
}


if(!isset($_SESSION['usuario'])){
    header("Location: ../view/login.php");
}


require_once '../controller/PostagemController.php';
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
              integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/css/froala_editor.pkgd.min.css" rel="stylesheet"
              type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <link rel="stylesheet" href="../assets/css/feed.css">
        <link href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Editar Resumo</title>
    </head>
    <body>
    <script src="https://cdn.tiny.cloud/1/tftq6c29ypcjjwz367z2v0lpnp0j4jyivdhadrk7f1sqfsk9/tinymce/5/tinymce.min.js"></script>
<script src="tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: '#conteudo',
    plugins: 'image code',
    toolbar: 'undo redo | image code',
    
    // without images_upload_url set, Upload tab won't show up
    images_upload_url: 'uploadTiny.php',
    
    // override default upload handler to simulate successful upload
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
      
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'uploadTiny.php');
      
        xhr.onload = function() {
            var json;
        
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
        
            json = JSON.parse(xhr.responseText);
        
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
        
            success(json.location);
        };
      
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
      
        xhr.send(formData);
    },
});
</script>
    <nav id="x">
        <div class="nav-wrapper blue-grey">
            <div class="container">
                <a href="feed.php" class="brand-logo black-text">Helpvest</a>
            </div>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li>
                    <div class="center row">
                        <div class="col s12" >
                            <div class="row">
                                <div class="input-field col s6 s12 black-text">
                                    <i class="black-text material-icons prefix">search</i>
                                    <input type="text" class="black-text">
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li><a href="create.php" class="black-text">Enviar Resumo</a></li>
                <li><a href="visualizarSeusPosts.php" class="black-text">Meus Resumos</a></li>
                <li><a href="#" class="black-text">Minhas Pastas</a></li>
            </ul>
        </div>
    </nav>
        <h3 style="text-align: center">Edite seu Resumo</h3>

        <?php
        $array = PostagemController::getAll();
        foreach ($array as $value) {
            if ($value[0] == $_GET["id"]) {
                $postagem = $value;
            }
        }

        ?>
    <div class="row">
        <div class="col s10 m10 l10 offset-s2 offset-m2 offset-l1 card-panel">
            <form class="col s12" method="post" action="../controller/PostagemController.php" id="formEdicao">
                <input type="hidden" name="update" value="<?= $postagem[0]; ?>"/>
                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">title</i>
                        <input id="tituloResumo" type="text" name="titulo" class="validate" required maxlength="35" value="<?= $postagem[1]; ?>">
                        <label for="tituloResumo">Título</label>
                    </div>
                    <div class="input-field col s12">
                        <i class="material-icons prefix">info</i>
                        <textarea style="height: 300px;" rows="4" cols="5" id="conteudo" name="conteudo" class="materialize-textarea"><?= $postagem[2]; ?></textarea>

                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">info</i>
                        <textarea rows="4" cols="5" id="breveDescricao" name="breveDescricao" class="materialize-textarea"><?= $postagem[5]; ?></textarea>
                        <label for="breveDescricao">Breve Descrição</label>
                    </div>
                    <div class="input-field col s6">
                        <select name="categoria" id="categoria">
                            <option value="" disabled selected>Escolha a categoria do resumo</option>
                            <option value="1" <?= $postagem[3] == 1 ? 'selected' : '' ?>>Matemática</option>
                            <option value="7" <?= $postagem[3] == 7 ? 'selected' : '' ?>>Biologia</option>
                            <option value="2" <?= $postagem[3] == 2 ? 'selected' : '' ?>>Física</option>
                            <option value="8" <?= $postagem[3] == 8 ? 'selected' : '' ?>>História</option>
                            <option value="3" <?= $postagem[3] == 3 ? 'selected' : '' ?>>Geografia</option>
                            <option value="9" <?= $postagem[3] == 9 ? 'selected' : '' ?>>Química</option>
                            <option value="4" <?= $postagem[3] == 4 ? 'selected' : '' ?>>Português</option>
                            <option value="10" <?= $postagem[3] == 10 ? 'selected' : '' ?>>Redação</option>
                            <option value="5" <?= $postagem[3] == 5 ? 'selected' : '' ?>>Filosofia</option>
                            <option value="11" <?= $postagem[3] == 11 ? 'selected' : '' ?>>Sociologia</option>
                            <option value="6" <?= $postagem[3] == 6 ? 'selected' : '' ?>>Espanhol</option>
                            <option value="12" <?= $postagem[3] == 12 ? 'selected' : '' ?>>Inglês</option>
                        </select>
                        <label>Categorias</label>

                    </div>
                    <button onclick="swal('Editado com sucesso!').then((value) => {formEdicao.submit()}); return false;" class="btn waves-light col s12" type="submit" name="action" value="submit">Editar
                    </button>


                </div>

        </div>
    </div>

    </form>

    <!--
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Formulário</h5>
                <p class="card-text">
                <div class="row container">
                    <form method="POST" action="../controller/PostagemController.php">
                        <input type="hidden" name="update" value="= $postagem[0]; ?>">
                        <div class="form-group">
                            <label for="titulo">Título</label>
                            <input type="text" name="titulo" class="form-control" id="titulo" value="// $postagem[1]; ?>" placeholder="Informe um título">
                        </div>
                        
                        <div class="form-group">
                            <label for="categoria">Selecione a categoria</label>
                            <select name="categoria" class="form-control" id="categoria">
                                <option value="1"  // $postagem[3] == 1 ? 'selected' : '' >Matemática</option>
                                <option value="7" = //$postagem[3] == 7 ? 'selected' : '' ?>>Biologia</option>
                                <option value="2"  //$postagem[3] == 2 ? 'selected' : '' ?>>Física</option>
                                <option value="8"  //$postagem[3] == 8 ? 'selected' : '' ?>>História</option>
                                <option value="3"  //$postagem[3] == 3 ? 'selected' : '' ?>>Geografia</option>
                                <option value="9"  //$postagem[3] == 9 ? 'selected' : '' ?>>Química</option>
                                <option value="4" //$postagem[3] == 4 ? 'selected' : '' ?>>Português</option>
                                <option value="10"  //$postagem[3] == 10 ? 'selected' : '' ?>>Redação</option>
                                <option value="5"  //$postagem[3] == 5 ? 'selected' : '' ?>>Filosofia</option>
                                <option value="11"  //$postagem[3] == 11 ? 'selected' : '' ?>>Sociologia</option>
                                <option value="6" //$postagem[3] == 6 ? 'selected' : '' ?>>Espanhol</option>
                                <option value="12"  //$postagem[3] == 12 ? 'selected' : '' ?>>Inglês</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="conteudo">Conteúdo</label>
                            <textarea class="form-control" name="conteudo" id="conteudo" rows="3"> $postagem[2]; ?></textarea>
                        </div>

                        <input class="btn btn-primary" type="submit" value="Submit">

                    </form>
                </div>
                </p>
                <a href="#" class="btn btn-primary">Go somewhere</a>

            </div>
        </div>
    -->
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/js/froala_editor.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="../assets/js/main.js" type="text/javascript"></script>
      
    </body>
</html>
