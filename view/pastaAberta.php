<?php
error_reporting(0);
if (!session_id()) {
    session_start();
}

if (!isset($_SESSION['usuario'])) {
    header("Location: ../view/login.php");
}

require_once '../controller/PastaController.php';
require_once '../controller/PostagemController.php';

?>

<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" href="../assets/css/create.css">
    <link href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <title>Resumo</title>
</head>

<body class="bg-image">

    <!-- Menu Horizontal topo -->
    <nav>
        <div class="nav-wrapper blue-grey">
            <div class="container">
                <a href="feed.php" class="brand-logo black-text">Helpvest</a>
            </div>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="create.php" class="black-text">Enviar Resumo</a></li>
                <li><a href="visualizarSeusPosts.php" class="black-text">Meus Resumos</a></li>
                <li><a href="pastasView.php" class="black-text">Minhas Pastas</a></li>
            </ul>
        </div>
    </nav>

    <?php
        $idCategoria = key($_GET);
        
        if (isset($_GET['pagina'])) {
            $active = $_GET['pagina'];
            $posts = PastaController::getAllPastaByCategoryAndUser($_SESSION['usuario'][0], $idCategoria, $active);
        } else {
            $active = 1;
            $posts = PastaController::getAllPastaByCategoryAndUser($_SESSION['usuario'][0], $idCategoria, $active);
        }
    ?>

    <div class="container">
        <div class="row">
            <div class="col s10 offset-s2" id="postagens">
                <?php
                $count = 0;
                if (!empty($posts["items"])) {
                    foreach ($posts["items"] as $value) {
                        $count++;
                        if ($count == 1) { ?>
                            <div class="row">
                            <?php
                                }
                            ?>
                            <div class="carta" value="<?= $value->id_postagem; ?>">
                                <div class="col s10 m4">
                                    <div class="card blue-grey darken-1">
                                        <div class="card-content white-text">
                                            <span class="card-title"><?= $value->titulo ?></span>
                                            <p><?= $value->breve_descricao ?></p>
                                        </div>
                                        <div class="card-action">
                                            <a href="#">Escrito por: <?= PostagemController::retornaUser($value->id_postagem)[0][0]; ?></a>
                                            <a class="modal-trigger" href="#modalLerMais<?= $value->id_postagem; ?>">Ler mais</a>
                                        </div>
                                        <div class="row">
                                            <div class="col s12">
                                                <a href="#modalRemove<?= $value->id_postagem; ?>" class="waves-effect waves-light btn modal-trigger red" style="width: 100%;">Remover da Pasta</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <!-- Modal de remoção -->
                                <div id="modalRemove<?= $value->id_postagem; ?>" class="modal">
                                    <div class="modal-content">
                                        <p>
                                            O item <b>"<?= $value->titulo; ?>"</b> será removido, você tem certeza que deseja remover?
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <form method="POST" action="../controller/PastaController.php">
                                            <input type="hidden" name="delete" value="delete">
                                            <input type="hidden" name="idPostagem" value="<?= $value->id_postagem; ?>">
                                            <input type="hidden" name="idCategoria" value="<?= $idCategoria; ?>">
                                            <input type="hidden" name="idUser" value="<?= $_SESSION['usuario'][0]; ?>">
                                            <button class="modal-close waves-effect waves-light btn-small blue" type="submit" name="action">
                                                Sim
                                            </button>
                                        </form>
                                        <a class="modal-close waves-effect waves-light btn-small red">Não</a>
                                    </div>
                                </div>

                            <!-- Modal Carta -->
                            <div id="modalLerMais<?= $value->id_postagem; ?>" class="modal" style=" width: 85% !important; height: 100% !important; max-height: 80%;  ">
                                <div class="row">
                                    <div class="col s7">
                                        <div class="modal-content center-align">
                                            <p>
                                                <b>Título: "<?= $value->titulo; ?>"</b>
                                            </p>
                                        </div>
                                        <div class="modal-footer center-align">
                                            <?= $value->conteudo; ?>
                                        </div>
                                    </div>
                                    <div class="col s5">
                                        <div id="autor container">
                                            <div class="row teal lighten-1">
                                                <div class="card-panel">
                                                    <p class="center-align"><b> Autor </b></p>
                                                    <div class="image-container center">
                                                        <img src="../upload/<?= PostagemController::retornaUser($value->id_postagem)[0][1]; ?>" class="circle responsive-img" style="max-width: 100px; max-height: 100px;">
                                                    </div>
                                                    <p class="center-align"><b>
                                                        <?= PostagemController::retornaUser($value->id_postagem)[0][0]; ?></b>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <?php
                            if ($count == 3) {
                                $count = 1;
                            }
                        }
                    } else {
                       // echo "<h2 class='center-align'> Não há resumos dessa disciplina disponíveis.</h2>";
                        echo "<script> swal('Não há resumos salvos nesta pasta').then((value) => {location.href='pastasView.php'}) </script>";
                    }
                    ?>
                    </div>
            </div>
        </div>
    </div>

    <!-- Paginação -->
    <div id="paginacao">
        <ul class="pagination center">
            <?php if ($active == 1) { ?>
                <li class="disabled"><a href="#">
                    <i class="material-icons">chevron_left</i></a>
                </li>
            <?php } else { ?>
                <li>
                    <a href="feed.php?pagina=<?= $active - 1; ?>">
                        <i class="material-icons">chevron_left</i>
                    </a>
                </li>
                <?php }

                for ($x = 0; $x < $posts['count']; $x++) {
                    if ($x + 1 == $active) {
                        ?>
                    <li class="waves-effect active">
                        <a href="feed.php?pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a></li>
                <?php
                    } else {
                        ?>
                    <li class="waves-effect">
                        <a href="feed.php?pagina=<?= $x + 1; ?>"><?= $x + 1; ?></a>
                    </li>
                <?php
                    }
                }

                if ($active == $posts['count']) { ?>
                <li class="waves-effect disabled">
                    <a href="#">
                        <i class="material-icons">chevron_right</i>
                    </a>
                </li>
            <?php } else { ?>
                <li class="waves-effect">
                    <a href="feed.php?pagina=<?= $active + 1; ?>">
                        <i class="material-icons">chevron_right</i>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
    </div>
    </div>



    <!-- Rodapé -->
    <footer id="rodape" class="page-footer blue-grey">
        <div class="container">
            <div class="row">
                <div class="col s6 offset-s2">
                    <h5 class="black-text">HELPVEST</h5>
                    <h6>Sua plataforma de estudos online!</h6>
                </div>
            </div>
        </div>
    </footer>

    <!-- JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="../assets/js/main.js" type="text/javascript"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  
</body>

</html>