<!-- <!doctype html>
<html>
<head>

</head>
<body>
<h1>Criação de questionário</h1>
<p>Pergunta</p><textarea></textarea>
<p> alternativa 1</p> <textarea></textarea>
<p> alternativa 2</p> <textarea></textarea>
<p> alternativa 3</p> <textarea></textarea>
<p> alternativa 4</p> <textarea></textarea>

<p>Alternativa certa:</p> <textarea></textarea>
<form method="post" type="submit" action="../controller/teste.php">
<input type="hidden" name="oi">
<select name="envia">
    <option value="1"> 1</option>
    <option value="2"> 2</option>
</select>
    <input type="submit">
</form>
</body>
</html>
-->
<!DOCTYPE html>
<html>
<head>
<script src="https://cdn.tiny.cloud/1/tftq6c29ypcjjwz367z2v0lpnp0j4jyivdhadrk7f1sqfsk9/tinymce/5/tinymce.min.js"></script>
<script src="tinymce/tinymce.min.js"></script>
 <script> src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></></script>
<script>
tinymce.init({
    selector: '#teste',
    plugins: 'image code',
    toolbar: 'undo redo | image code',
    
    // without images_upload_url set, Upload tab won't show up
    images_upload_url: 'uploadTiny.php',
    
    // override default upload handler to simulate successful upload
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
      
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'uploadTiny.php');
      
        xhr.onload = function() {
            var json;
        
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
        
            json = JSON.parse(xhr.responseText);
        
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
        
            success(json.location);
        };
      
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
      
        xhr.send(formData);
    },
});
</script>
</head>
<body>
  <textarea id="teste">Next, use our Get Started docs to setup Tiny!</textarea>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<form id="formQuest" method="POST" action="../controller/QuestionarioController.php">
    <div style="margin-left: 7px;" class="row">
        <h3 style="text-align: center;">Criação de questionário</h3>
        <div class="col s4">

            alternativa 1 <textarea name="alternativa[]"></textarea>
            alternativa 2 <textarea name="alternativa[]"></textarea>
            alternativa 3 <textarea name="alternativa[]"></textarea>
            alternativa 4 <textarea name="alternativa[]"></textarea>

            <select name="selecionar">
                <option value="" disabled selected>Escolha resposta certa</option>
                <option value="0">1</option>
                <option value="1">2</option>
                <option value="2">3</option>
                <option value="3">4</option>
            </select>
            <label>Alternativas</label>
        </div>
        <div class="col s7">
            <input type="hidden" name="passaTudoQuest"/>
            <input type="hidden" name="passaIdResumo" value="<?= $value[0]; ?>"/>
            <input type="hidden" name="passaIdUser" value="<?= $value[4]; ?>"/>
            Pergunta<textarea name="pergunta" style="height: 300px;"></textarea>
            <button type="submit">Envia</button>
            
         </div>
    </div>
</form>

<script>

</script>
</body>
</html>