<?php

if (!session_id()) {
    session_start();
}

if(!isset($_SESSION['usuario'])){
    header("Location: ../view/login.php");
}

?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <link rel="stylesheet" href="../assets/css/create.css">
        <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Inconsolata&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Resumo</title>
    </head>

    <body class="bg-image">
        
        <!-- Menu Horizontal topo -->
        <nav>
            <div class="nav-wrapper blue-grey">
                <div class="container">
                    <a href="feed.php" class="brand-logo black-text">Helpvest</a>
                </div>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="create.php" class="black-text">Enviar Resumo</a></li>
                    <li><a href="visualizarSeusPosts.php" class="black-text">Meus Resumos</a></li>
                    <li><a href="pastasView.php" class="black-text">Minhas Pastas</a></li>
                </ul>
            </div>
        </nav>

        <div class="container center-vertical">
            <div class="row">
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=1">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Matemática
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=2">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Física
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=3">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Geografia
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=4">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Português
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=5">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Filosofia
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=6">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Espanhol
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=7">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Biologia
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=8">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>História
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=9">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Química
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=10">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Redação
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=11">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Sociologia
                    </a>
                </div>
                <div class="col s2">
                    <a class="black-text" href="../controller/pastaController.php?message=12">
                        <i class="fas fa-folder fa-8x c-green-lighten-3"></i>Inglês
                    </a>
                </div>
            </div>
        </div>

        <footer id="rodape" class="page-footer blue-grey">
            <div class="container">
                <div class="row">
                    <div class="col s6 offset-s2">
                        <h5 class="black-text">HELPVEST</h5>
                        <h6>Sua plataforma de estudos online!</h6>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Compiled and minified JavaScript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <script src="../assets/js/main.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@3.0.0-beta.1/js/froala_editor.pkgd.min.js"></script>

        <script> 
            $(function () {
                new FroalaEditor('#conteudo', {
                    // Set the file upload URL.
                    imageUploadURL: 'UploadImagemFroala.php',

                    imageUploadParams: {
                        id: 'my_editor'
                    }
                });
            });
        </script>

    </body>
</html>
