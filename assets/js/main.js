
$(document).ready(function() {
    //alert('oi');
    $('select').material_select();
    $('.chips').material_chip();
    $('.modal').modal();

    $('.itens').click(function() {
        $('.carta').hide();
        $('#paginacao').hide();
        var idCategoriaMenuLateral = parseInt($(this).attr('value'), 10);

        $('.carta').each(function() {
            idCategoriaCards = parseInt($(this).attr('value'), 10);
            if (idCategoriaCards == idCategoriaMenuLateral) {
                $(this).show();
            }
        });
    });
   
    });

    $("#selecionarAlternativa").click(function() {
        $(".group").each(function(data) {
            $(this).parent().css("background", "white");
            if ($(this).prop("checked")) {
                if ($(this).parent().attr('name') == "correta") {
                    $(this).parent().css("background", "green");
                } else {
                    $(this).parent().css("background", "red");
                }
            }
        });
    });
