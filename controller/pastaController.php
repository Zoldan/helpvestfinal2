<?php

if (!session_id()) {
    session_start();
}

require_once "../model/pasta.php";

if (isset($_POST['adicionaPasta'])) {
    PastaController::adicionarPasta();
}

if (isset($_POST['delete'])) {
    PastaController::delete();
}

if (isset($_GET['message'])) {
    PastaController::mudarRota();
}
class PastaController
{

    public static function adicionarPasta()
    {
        $idPost = filter_var($_POST['idPost'], FILTER_SANITIZE_NUMBER_INT);
        $idCategoria = filter_var($_POST['idCategoria'], FILTER_SANITIZE_NUMBER_INT);
        $idUser = filter_var($_POST['idUser'], FILTER_SANITIZE_NUMBER_INT);
        
        $pasta = new Pasta($idPost, $idCategoria, $idUser, null);
        $pasta->adicionarPasta();

        header("Location: ../view/feed.php");
    }

    /**
     * 0 - Resumo pertencente ao usuário logado
     * 1 - Resumo já foi adicionado na pasta deste usuário
     * 2 - Resumo válido
     */
    public static function verificarStatus($idUser, $idPost, $idDonoPost)
    {
        if ($idUser == $idDonoPost) {
            return 0;
        }

        $pasta = new Pasta($idPost, null, $idUser, null);
        return $pasta->verificarStatus();
    }

    public static function getAllPastaByCategoryAndUser($idUser, $idCategoria, $pagina)
    {
        $limit = 6;
        $pagina = ($pagina - 1) * $limit;
        $pasta = new Pasta(null, $idCategoria, $idUser, null);
        $count = count($pasta->getAllPastaByCategoryAndUser());
        $items = $pasta->pagination($pagina, $limit);
        return [
            "items" => $items,
            "count" => ceil($count/$limit),
        ];
    }

    public static function delete() 
    {
        print_r($_POST);
        $idPost = filter_var($_POST['idPostagem'], FILTER_SANITIZE_NUMBER_INT);
        $idUser = filter_var($_POST['idUser'], FILTER_SANITIZE_NUMBER_INT);
        $idCategoria = filter_var($_POST['idCategoria'], FILTER_SANITIZE_NUMBER_INT);
        $pasta = new Pasta($idPost, null, $idUser, null);
        $pasta->delete();
        header("Location: ../view/pastaAberta.php?".$idCategoria);
    }
    
    public static function mudarRota(){
        header("Location: ../view/pastaAberta.php?".$_GET['message']);
    }
}
