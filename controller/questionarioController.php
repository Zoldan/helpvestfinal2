<?php

require_once '../model/Questionario.php';
require_once '../model/Pergunta.php';
require_once '../model/Alternativa.php';

if (!session_id()) {
    session_start();
}

if (isset($_POST["cria_questionario"])) {
    QuestionarioController::novoQuestionario();
}

if (isset($_POST["edita_questionario"])) {
    QuestionarioController::editarQuestionario();
}

if (isset($_POST["delete"])) {
    QuestionarioController::deletarQuestionario($_POST["idQuestionario"], $_POST["idPergunta"], $_POST["idAlternativa"]);
}

class QuestionarioController
{

    public static function novoQuestionario()
    {
        $idPostagem = filter_var($_POST['idPostagem'] ,FILTER_SANITIZE_NUMBER_INT);
        $idUser = filter_var($_POST['idUser'], FILTER_SANITIZE_NUMBER_INT);
        $opcaoCerta = filter_var($_POST['opcao_correta'], FILTER_SANITIZE_NUMBER_INT);
        $pergunta = filter_var($_POST['pergunta'], FILTER_SANITIZE_STRING);

        $questionario = new Questionario($idPostagem, $idUser, $opcaoCerta);
        $questionarioId = $questionario->novoQuestionario();

        $pergunta = new Pergunta($questionarioId, $pergunta);
        $perguntaId = $pergunta->novaPergunta();

        foreach ($_POST["alternativa"] as $value) {
            $alternativa = new Alternativa($perguntaId, $value);
            $alternativa->novaAlternativa();
        }

        header("Location: ../view/visualizarSeusPosts.php");
    }

    public static function deletarQuestionario($questionarioId, $perguntaId, $alternativasId)
    {
        foreach ($alternativasId as $value) {
            $alternativa = new Alternativa(null, null);
            $alternativa->delete($value);
        }

        $pergunta = new Pergunta(null, null);
        $pergunta->delete($perguntaId);

        $questionario = new Questionario(null, null, null);
        $questionario->delete($questionarioId);

        header("Location: ../view/visualizarSeusPosts.php");
    } 

    public static function editarQuestionario()
    {
        $idPostagem = filter_var($_POST['idPostagem'] ,FILTER_SANITIZE_NUMBER_INT);
        $idUser = filter_var($_POST['idUser'], FILTER_SANITIZE_NUMBER_INT);
        $opcaoCerta = filter_var($_POST['opcao_correta'], FILTER_SANITIZE_NUMBER_INT);
        
        $idQuestionario = filter_var($_POST['idQuestionario'], FILTER_SANITIZE_NUMBER_INT);
        $idPergunta = filter_var($_POST['idPergunta'], FILTER_SANITIZE_NUMBER_INT);
        $idAlternativa[0] = filter_var($_POST['idAlternativa1'], FILTER_SANITIZE_NUMBER_INT);
        $idAlternativa[1] = filter_var($_POST['idAlternativa2'], FILTER_SANITIZE_NUMBER_INT);
        $idAlternativa[2] = filter_var($_POST['idAlternativa3'], FILTER_SANITIZE_NUMBER_INT);
        $idAlternativa[3] = filter_var($_POST['idAlternativa4'], FILTER_SANITIZE_NUMBER_INT);

        $pergunta = filter_var($_POST['pergunta'], FILTER_SANITIZE_STRING);

        $questionario = new Questionario($idPostagem, $idUser, $opcaoCerta);
        $questionario->editarQuestionario($idQuestionario);
        
        $pergunta = new Pergunta($idQuestionario, $pergunta);
        $pergunta->editarPergunta($idPergunta);

        foreach ($_POST["alternativa"] as $key => $value) {
            $alternativa = new Alternativa($idPergunta, $value);
            $alternativa->editarAlternativa($idAlternativa[$key]);
        }

        header("Location: ../view/visualizarSeusPosts.php");
    }

    public static function verificarExistencia($idPostagem)
    {
        $questionario = new Questionario($idPostagem, null, null);
        return $questionario->verificarExistencia();
    }

    public static function mostrarQuestionario($idPostagem)
    {
        $questionario = new Questionario($idPostagem, null, null);
        return $questionario->mostrarQuestionario();
    }

    public static function mostrarPergunta($idQuestionario)
    {
        $pergunta = new Pergunta($idQuestionario, null);
        return $pergunta->mostrarPergunta();
    }

    public static function mostrarAlternativas($idPergunta)
    {
        $alternativa = new Alternativa($idPergunta, null);
        return $alternativa->mostrarAlternativas();
    }

}