<?php

if (!session_id()) {
    session_start();
}

require_once "../model/Usuario.php";

if (isset($_POST['cadastrar'])) {
    UsuarioController::cadastrarUsuario();
}

if (isset($_POST['autenticar'])) {
    UsuarioController::autenticarUsuario();
}

if (isset($_POST["alterarImagem"])) {
    UsuarioController::alterarImagem();
}

if (isset($_GET['message'])){
    UsuarioController::sair();
}


class UsuarioController {

    public static function cadastrarUsuario() {

        $nome = filter_var($_POST['nome'], FILTER_SANITIZE_STRING);
        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $senha = filter_var($_POST['senha'], FILTER_SANITIZE_STRING);
        $confirmarSenha = filter_var($_POST['confirmarSenha'], FILTER_SANITIZE_STRING);
        
        if (!$senha == $confirmarSenha) {
            header("Location: ../view/login.php");
        }

        $usuario = new Usuario($nome, $senha, $email);
        $usuario->insert();
        header("Location: ../view/login.php");
    }

    public static function autenticarUsuario() {

        $email = filter_var($_POST['emailLogin'], FILTER_SANITIZE_EMAIL);
        $senha = filter_var($_POST["senhaLogin"], FILTER_SANITIZE_STRING);

        $usuario = new Usuario(null, $senha, $email);
        $dadosUsuario = $usuario->autenticar();

        if (!empty($dadosUsuario)) {
            $_SESSION['usuario'] = $dadosUsuario;
            header("Location: ../view/feed.php");
        } else {
            header("location:../view/login.php");
        }
    }

    public static function alterarImagem()
    {
        $posicao = strripos($_FILES['arquivo']['name'], ".");
        $nomeImagem = substr($_FILES['arquivo']['name'], 0, $posicao);
        $tipoImagem = substr($_FILES['arquivo']['name'], $posicao);

        $newNomeImagem = $nomeImagem . $_POST['idUser'] . $tipoImagem;
        $usuario = new Usuario(null, null, null);

        $caminho = '../upload/' . $newNomeImagem;

        if ($_FILES['arquivo']['type'] == "image/gif" || $_FILES['arquivo']['type'] == "image/jpeg" || $_FILES['arquivo']['type'] == "image/jpg" || $_FILES['arquivo']['type'] == "image/png") {
            if ($_FILES['arquivo']['size'] < 10485760 && move_uploaded_file($_FILES['arquivo']['tmp_name'], $caminho)) {
                $usuario->alterarImagem($newNomeImagem, $_POST['idUser']);
                $_SESSION["usuario"][4] = $newNomeImagem;
            }
        }


        header("Location: ../view/feed.php");

    }
    public static function sair (){
        session_destroy();
        header("Location: ../view/login.php");
    }


}
