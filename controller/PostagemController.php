<?php

if (!session_id()) {
    session_start();
}

require_once '../model/Postagem.php';

if (isset($_POST["insert"])) {
    PostagemController::insert();
}

if (isset($_POST["update"])) {
    PostagemController::update();
}

if (isset($_POST["delete"])) {
    PostagemController::delete();
}

if (isset($_POST["curtir"])){
    PostagemController::curtir($_POST["idPostagem"]);
    
}

if (isset($_POST["descurtir"])){
    PostagemController::descurtir($_POST["idPostagem"]);
}

if(isset($_REQUEST["pesquisar"])){
   // print_r($_POST["pesquisar"]);
    //print_r($_POST["pagina"]);
    PostagemController::pagination($_REQUEST["pagina"]);
}


class PostagemController {

    public static function insert()
    {

        $titulo = filter_var($_POST['titulo'], FILTER_SANITIZE_STRING);
        $conteudo = filter_var($_POST['conteudo']);
        $idCategoria = filter_var($_POST['categoria'], FILTER_SANITIZE_NUMBER_INT);
        $breveDescricao = filter_var($_POST['breveDescricao'], FILTER_SANITIZE_STRING);
        $postagem = new Postagem($titulo, $conteudo, $_SESSION["usuario"][0], $idCategoria, $breveDescricao);
        $postagem->insert();
        //print_r($_POST);
        $_SESSION['message'] = "Cadastrado com sucesso!";
        header("location: ../view/feed.php");
    }

    public static function update()
    {

        $titulo = filter_var($_POST['titulo'], FILTER_SANITIZE_STRING);
        $conteudo = filter_var($_POST['conteudo']);
        $idCategoria = filter_var($_POST['categoria'], FILTER_SANITIZE_NUMBER_INT);
        $breveDescricao = filter_var($_POST['breveDescricao'], FILTER_SANITIZE_STRING);

        $postagem = new Postagem($titulo, $conteudo, $_SESSION["usuario"][0], $idCategoria, $breveDescricao);
        $postagem->update($_POST["update"]);

        header("location: ../view/visualizarSeusPosts.php");
    }

    public static function getAll($idCategoria = null)
    {
        
        $postagem = new Postagem(null, null, null, null, null);
        $aux = $postagem->getAll($idCategoria);
        return $aux ? $aux : [];

    }

    public static function pagination($pagina, $idCategoria=null)
    {
        $limit = 6;
        $pagina = ($pagina - 1) * $limit;
        $postagem = new Postagem(null, null, null, null, null);
        //print_r($_POST);
        if (!isset($_REQUEST["pesquisar"]) or $_REQUEST['pesquisar'] === ''){
        $items = $postagem->pagination($pagina, $limit, $idCategoria);
        $count = count(self::getAll($idCategoria));
        return [
            "items" => $items,
            "count" => ceil($count/$limit),
        ];
    } else {
        $items = $postagem->paginationPesquisa($pagina, $limit, $_REQUEST["pesquisar"]);
        $count = count(self::pesquisarPostagem($_REQUEST["pesquisar"]));
        return [
            "items" => $items,
            "count" => ceil($count/$limit),
        ];
    }
    }

    public static function delete()
    {

        $postagem = new Postagem(null, null, null, null, null);
        $postagem->delete($_POST["delete"]);
        header("location: ../view/visualizarSeusPosts.php");

    }

    public static function buscarCategoriaPorPost($idCategoria)
    {

        $postagem = new Postagem(null, null, null, null, null);
        return $postagem->buscarCategoriaPorPost($idCategoria);

    }

    public static function retornaUser ($idPostagem){
        $postagem = new Postagem(null, null, null, null, null);
        return $postagem->retornaUserDoPost($idPostagem);
    }

    public static function curtir ($idPostagem){
        $postagem = new Postagem (null, null, null, null, null);
        $postagem->curtir($idPostagem, $_SESSION["usuario"][0]);
    }

    public static function descurtir ($idPostagem){
        $postagem = new Postagem (null, null, null, null, null);
        $postagem->descurtir($idPostagem, $_SESSION["usuario"][0]);
    }

    public static function qtdCurtidas($idPostagem)
    {
        $postagem = new Postagem(null, null, null, null, null);
        return $postagem->mostrarQuantidadeDeCurtidas($idPostagem);
    }

    public static function possuiCurtida($idPostagem, $idUser)
    {
            
        $postagem = new Postagem(null, null, null, null, null);
        $retorno = $postagem->possuiCurtida($idPostagem, $idUser);
        echo $retorno;
        return $retorno;
    }

    public static function pesquisarPostagem ($texto){
        $postagem = new Postagem (null, null, null, null, null);
        $aux = $postagem->pesquisar($texto);
        return $aux ? $aux : [];
    }

}