<?php

if (!session_id()) {
    session_start();
}

require_once 'Conexao.php';

class Pasta
{
    private $id;
    private $idPostagem;
    private $idCategoria;
    private $idUser;

    public function __construct($idPost, $idCategoria, $idUser, $id)
    {
        $this->id = $id;
        $this->idPostagem = $idPost;
        $this->idUser = $idUser;
        $this->idCategoria = $idCategoria;
    }

    public function adicionarPasta()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("INSERT INTO pasta (idcategoria, idresumo, iduser) VALUES (?,?,?) ");
        $stmt->bindParam(1, $this->idCategoria);
        $stmt->bindParam(2, $this->idPostagem);
        $stmt->bindParam(3, $this->idUser);
        $stmt->execute();
    }

    public function verificarStatus()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM pasta WHERE iduser = ? AND idresumo = ?");
        $stmt->bindParam(1, $this->idUser);
        $stmt->bindParam(2, $this->idPostagem);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            return 1;
        }

        return 2;
    }

    public function delete()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("DELETE FROM pasta WHERE idresumo = ? AND iduser = ?");
        $stmt->bindParam(1, $this->idPostagem);
        $stmt->bindParam(2, $this->idUser);
        $stmt->execute();
    }

    public function getAllPastaByCategoryAndUser()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM postagem, pasta WHERE postagem.id_postagem = pasta.idresumo AND pasta.idcategoria = ? AND pasta.iduser = ?");
        $stmt->bindParam(1, $this->idCategoria);
        $stmt->bindParam(2, $this->idUser);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {

            $results[] = $row;
        }

        if (isset($results)) {

            return $results;
        }

        return null;
    }

    public function pagination($pagina, $limit)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM postagem, pasta WHERE postagem.id_postagem = pasta.idresumo AND pasta.idcategoria = ? AND pasta.iduser = ? order by postagem.id_postagem desc limit $pagina, $limit");
        $stmt->bindParam(1, $this->idCategoria);
        $stmt->bindParam(2, $this->idUser);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {

            $results[] = $row;
        }

        if (isset($results)) {

            return $results;
        }

        return null;
    }
}