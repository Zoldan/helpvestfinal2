<?php

if (!session_id()) {
    session_start();
}

include_once 'Conexao.php';

class Usuario {

    private $nomeUser;
    private $senhaUser;
    private $emailUser;
    private $fotoUser;

    function __construct($nomeUser, $senhaUser, $emailUser) {
        $this->nomeUser = $nomeUser;
        $this->senhaUser = $senhaUser;
        $this->emailUser = $emailUser;
    }

    public function insert() {

        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("INSERT INTO usuario (nome_user, senha_user, email_user) VALUES (?, ?, ?)");
        $stmt->bindParam(1, $this->nomeUser);
        $stmt->bindValue(2, password_hash("$this->senhaUser", PASSWORD_DEFAULT));
        $stmt->bindParam(3, $this->emailUser);
        $stmt->execute();
    }

    public function buscarUsuarioPorEmail($email) {

        // fazer depois
    }

    public function autenticar() {

        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM usuario WHERE email_user = ?");
        $stmt->bindParam(1, $this->emailUser);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                if (password_verify("$this->senhaUser", $row->senha_user)) {
                    $results = array($row->id_user, $row->nome_user, $row->email_user, $row->senha_user, $row->nome_imagem);
                }
            }
        }

        if (!isset($results)) {
            $results = null;
        }
        return $results;
    }

    public function alterarImagem($nomeImagem, $idUser)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("UPDATE usuario SET nome_imagem = ? WHERE id_user = ? ");
        $stmt->bindParam(1, $nomeImagem);
        $stmt->bindParam(2, $idUser);
        $stmt->execute();
    }


}
