<?php

require_once 'Conexao.php';

class Alternativa
{

    private $idPergunta;
    private $texto;

    public function __construct($idPergunta, $texto)
    {
        $this->idPergunta = $idPergunta;
        $this->texto = $texto;
    }

    public function novaAlternativa()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("INSERT INTO alternativas (idpergunta, texto) values (?, ?)");
        $stmt->bindParam(1, $this->idPergunta);
        $stmt->bindParam(2, $this->texto);
        $stmt->execute();
        return $conexao->lastInsertId();
    }

    public function editarAlternativa($idAlternativa)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("UPDATE alternativas SET idpergunta = ? , texto = ? WHERE idalternativa = ?");
        $stmt->bindParam(1, $this->idPergunta);
        $stmt->bindParam(2, $this->texto);
        $stmt->bindParam(3, $idAlternativa);
        $stmt->execute();
    }

    public function delete($id)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("DELETE FROM alternativas WHERE idalternativa = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
    }

    public function mostrarAlternativas()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM alternativas WHERE alternativas.idpergunta = ?");
        $stmt->bindParam(1, $this->idPergunta);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results[] = [
                $row->idalternativa,
                $row->idpergunta,
                $row->texto
            ];
        }

        return $results;
    }
}