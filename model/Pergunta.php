<?php

require_once 'Conexao.php';

class Pergunta
{

    private $idquest;
    private $texto;

    public function __construct($idquest, $texto)
    {
        $this->idquest = $idquest;
        $this->texto = $texto;
    }

    public function novaPergunta()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("INSERT INTO Pergunta (idquestionario, texto) VALUES (?, ?)");
        $stmt->bindParam(1, $this->idquest);
        $stmt->bindParam(2, $this->texto);
        $stmt->execute();
        return $conexao->lastInsertId();
    }

    public function mostrarPergunta()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM pergunta WHERE pergunta.idquestionario = ?");
        $stmt->bindParam(1, $this->idquest);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results = [
                $row->idpergunta,
                $row->idquestionario,
                $row->texto
            ];
        }

        return $results;
    }

    public function editarPergunta($idPergunta)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("UPDATE pergunta SET idquestionario = ? , texto = ? WHERE idpergunta = ?");
        $stmt->bindParam(1, $this->idquest);
        $stmt->bindParam(2, $this->texto);
        $stmt->bindParam(3, $idPergunta);
        $stmt->execute();
    }

    public function delete($id)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("DELETE FROM pergunta WHERE idpergunta = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
    }


}