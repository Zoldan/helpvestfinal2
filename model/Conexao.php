<?php

if (!session_id()) {
    session_start();
}

class Conexao {

    private $servidor = "localhost";
    private $database = "helpvest";
    private $user = "root";
    private $password = "";

    public function conecta() {

        $conn = null;
        $conn = new PDO("mysql:host=$this->servidor;dbname=$this->database", $this->user, $this->password);
        return $conn;
    }

    public function desconecta(&$conn) {
        $conn = null;
    }

}
