<?php

if (!session_id()) {
    session_start();
}

require_once 'Conexao.php';

class Postagem {

    private $titulo;
    private $conteudo;
    private $idUser;
    private $idCategoria;
    private $breveDescricao;

    function __construct($titulo, $conteudo, $idUser, $idCategoria, $breveDescricao)
    {
        $this->titulo = $titulo;
        $this->conteudo = $conteudo;
        $this->idUser = $idUser;
        $this->idCategoria = $idCategoria;
        $this->breveDescricao = $breveDescricao;
    }


    public function insert()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("INSERT INTO postagem (titulo, conteudo, id_categoria, id_user, breve_descricao) VALUES (?, ?, ?, ?,?)");
        $stmt->bindParam(1, $this->titulo);
        $stmt->bindParam(2, $this->conteudo);
        $stmt->bindParam(3, $this->idCategoria);
        $stmt->bindParam(4, $this->idUser);
        $stmt->bindParam(5, $this->breveDescricao);
        $stmt->execute();
    }

    public function update($id)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("UPDATE postagem SET titulo = ? , conteudo = ? , id_categoria = ? , id_user = ?, breve_Descricao = ? WHERE id_postagem = ? ");
        $stmt->bindParam(1, $this->titulo);
        $stmt->bindParam(2, $this->conteudo);
        $stmt->bindParam(3, $this->idCategoria);
        $stmt->bindParam(4, $this->idUser);
        $stmt->bindParam(5, $this->breveDescricao);
        $stmt->bindParam(6, $id);
        $stmt->execute();
    }
    
    public function delete($id)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("DELETE FROM postagem WHERE id_postagem = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
    }
    
    public function getOne($id)
    {
        
        /* Aqui será feito o SQL: SELECT * FROM postagem WHERE id_postagem = $id
         * Esse método será acionado quando o usuário clicar no botão visibility em "visualizar seus Posts"
         */
    }

    public function getAll($idCategoria = null)
    {

        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        if ($idCategoria === null or $idCategoria === ''){
        $stmt = $conexao->prepare("SELECT * FROM postagem order by id_postagem desc");
        } else {
        $stmt = $conexao->prepare("SELECT * FROM postagem where id_categoria = ? order by id_postagem desc");
        $stmt->bindParam(1, $idCategoria);
        }
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results[] = array($row->id_postagem, $row->titulo, $row->conteudo, $row->id_categoria, $row->id_user, $row->breve_descricao);
        }

        if (isset($results)) {
            return $results;
        }

        return null;
    }

    public function retornaUserDoPost($idPostagem){

        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("select nome_user,nome_imagem from postagem, usuario where postagem.id_user = usuario.id_user and postagem.id_postagem = ?");
        $stmt->bindParam(1, $idPostagem);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results[] = array($row->nome_user, $row->nome_imagem);
        }
        if (isset($results)){
            return $results;
        }else{
            return null;
        }

    }

    public function paginationPesquisa ($pagina, $limit, $texto){
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * from postagem where breve_descricao like ? OR titulo like ? order by id_postagem desc limit $pagina, $limit");
        $aux = "%" . $texto . "%";
        $stmt->bindParam(1, $aux);
        $stmt->bindParam(2, $aux);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {

            $results[] = $row;
        }

        if (isset($results)) {
            
            return $results;
        }


        return null;
    }

    public function pagination($pagina, $limit, $idCategoria=null)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        if ($idCategoria === null or $idCategoria === ''){
        $stmt = $conexao->prepare("SELECT * FROM postagem order by id_postagem desc limit $pagina, $limit");
        } else {
        $stmt = $conexao->prepare("SELECT * FROM postagem where id_categoria = ? order by id_postagem desc limit $pagina, $limit");
        $stmt->bindParam(1, $idCategoria);
        }
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {

            $results[] = $row;
        }

        if (isset($results)) {

            return $results;
        }


        return null;
    }

    public function buscarCategoriaPorPost($idPost)
    {

        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM postagem, categoria WHERE "
                . "postagem.id_categoria = categoria.id_categoria AND categoria.id_categoria = ? AND postagem.id_user = ?");
        $stmt->bindParam(1, $idPost);
        $stmt->bindParam(2, $_SESSION["usuario"][0]);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results = $row->nome;
        }

        if (isset($results)) {
            return $results;
        }

        return null;
    }

    public function curtir ($idPostagem, $user){
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("INSERT INTO curtida (id_resumo, id_usuario) values (?,?)");
        $stmt->bindParam(1, $idPostagem);
        $stmt->bindParam(2, $user);
        $stmt->execute();
        echo $idPostagem . " " . $user;
    }

    public function descurtir ($idPostagem, $user){
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("DELETE FROM curtida where id_usuario = ? and id_resumo = ?");
        $stmt->bindParam(1, $user);
        $stmt->bindParam(2, $idPostagem);
        $stmt->execute();
    }

    public function mostrarQuantidadeDeCurtidas ($idPostagem){
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare ("SELECT COUNT(*) as total from curtida where id_resumo = ?");
        $stmt->bindParam(1, $idPostagem);
        $stmt->execute();

        if ($stmt) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                $results = $row->total;
            }
        }
        if (isset($results)) {
            return $results;
        }
    
        return null;
    }
    public function possuiCurtida ($idPostagem, $idUser){

        $conexao = new conexao();
          $conexao = $conexao->conecta();
          $stmt = $conexao->prepare("SELECT * FROM curtida where id_resumo = ? and id_usuario = ?");
          $stmt->bindParam(1, $idPostagem);
          $stmt->bindParam(2, $idUser);
          $stmt->execute();
  
          if ($stmt) {
              while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                  $results[] = array($row->id_resumo, $row->id_usuario);
              }
          }
  
          if (isset($results)) {
              return true;
          } else {
              return false;
          }
      }  
    
     public function pesquisar ($texto){

        $conexao = new conexao();
        $conexao = $conexao->conecta();

        $stmt = $conexao->prepare("select * from postagem where breve_descricao like ? OR titulo like ? order by id_postagem");
        $aux = "%" . $texto . "%";
        $stmt->bindParam(1, $aux);
        $stmt->bindParam(2, $aux);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results[] = array($row->id_postagem, $row->titulo, $row->conteudo, $row->id_categoria, $row->id_user, $row->breve_descricao);
        }

        if (isset($results)) {
            return $results;
        }

        return null;
    }

}
