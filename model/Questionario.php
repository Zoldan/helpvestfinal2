<?php

if (!session_id()) {
    session_start();
}

require_once 'Conexao.php';

class Questionario
{
    private $idResumo;
    private $idUser;
    private $idRespCerta;

    public function __construct($idresumo, $iduser, $idrespcerta)
    {
        $this->idResumo = $idresumo;
        $this->idUser = $iduser;
        $this->idRespCerta = $idrespcerta;
    }

    public function novoQuestionario ()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("INSERT INTO questionario (idresumo, iduser, idrespcerta) VALUES (?, ?, ?)");
        $stmt->bindParam(1, $this->idResumo);
        $stmt->bindParam(2, $this->idUser);
        $stmt->bindParam(3, $this->idRespCerta);
        $stmt->execute();
        return $conexao->lastInsertId();
    }

    public function delete($id)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("DELETE FROM questionario WHERE idquest = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
    }

    public function verificarExistencia()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM questionario WHERE questionario.idresumo = ?");
        $stmt->bindParam(1, $this->idResumo);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            return true;
        }

        return false;
    }

    public function mostrarQuestionario()
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("SELECT * FROM questionario WHERE questionario.idresumo = ?");
        $stmt->bindParam(1, $this->idResumo);
        $stmt->execute();

        while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
            $results = [
                $row->idquest,
                $row->idresumo,
                $row->iduser,
                $row->idrespcerta
            ];
        }

        return $results;
    }

    public function editarQuestionario($idQuestionario)
    {
        $conexao = new Conexao();
        $conexao = $conexao->conecta();
        $stmt = $conexao->prepare("UPDATE questionario SET idresumo = ? , iduser = ? , idrespcerta = ? WHERE idquest = ?");
        $stmt->bindParam(1, $this->idResumo);
        $stmt->bindParam(2, $this->idUser);
        $stmt->bindParam(3, $this->idRespCerta);
        $stmt->bindParam(4, $idQuestionario);
        $stmt->execute();
    }

}